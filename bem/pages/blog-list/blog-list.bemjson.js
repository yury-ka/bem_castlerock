module.exports = {
    block: 'page',
    title: 'Список блог',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../_common/header.bemjson.js')(),

        {
            block: 'block',
            mods: {theme: 'light'},
            attrs: {
                'data-theme': 'light'
            },
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'breadcrumb'
                        },
                        {
                            block: 'title',
                            mods: {first:true},
                            mix: [{block: 'h1'}],
                            tag: 'h1',
                            content: 'PAIN – Зал ожидания'
                        },
                        {
                            block: 'row',
                            mix: [{block: 'mbl'}],
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '8'},
                                    content: [
                                        {
                                            block: 'search',
                                            mods: {select: true},
                                            icon: 'fa',
                                            placeholder: 'Искать в блоге'
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '2'},
                                    content: [
                                        {
                                            block: 'btn',
                                            mods: {color: 'primary', select: true},
                                            attrs: {
                                                'data-toggle': 'collapse',
                                                href: '#selection-collapse'
                                            },
                                            content: 'Все темы, все теги'
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '2'},
                                    content: [
                                        'Подписаться'
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'collapse',
                            attrs: {
                                id: 'selection-collapse'
                            },
                            content: [
                                require('../_common/selection.bemjson.js'),
                            ]
                        },

                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '3', sm: '4'},
                                    content: [
                                        {
                                            block: 'sidebar',
                                            content: [
                                                {
                                                    block: 'rubric',
                                                    content: [
                                                        {
                                                            elem: 'title',
                                                            content: 'Рок-блог'
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: [
                                                                {
                                                                    elem: 'name',
                                                                    content: 'Леонид Фридман рассказывает'
                                                                },
                                                                {
                                                                    elem: 'count',
                                                                    content: '18'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: [
                                                                {
                                                                    elem: 'name',
                                                                    content: 'Мир Castle rock'
                                                                },
                                                                {
                                                                    elem: 'count',
                                                                    content: '17'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: [
                                                                {
                                                                    elem: 'name',
                                                                    content: 'Отчеты с концертов'
                                                                },
                                                                {
                                                                    elem: 'count',
                                                                    content: '26'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: [
                                                                {
                                                                    elem: 'name',
                                                                    content: 'Про рок'
                                                                },
                                                                {
                                                                    elem: 'count',
                                                                    content: '250'
                                                                }
                                                            ]
                                                        },
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '9', sm: '8'},
                                    content: [
                                        require('../_common/blog-preview-line.bemjson.js')(),
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'row',
                            mix: [{block: 'mbl'}],
                            content: [
                                {
                                    elem: 'col',
                                    mods: {lg: 6, xs: 12},
                                    content: [
                                        {
                                            block: 'share',
                                            mods: {right: true},
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Делись:'
                                                },
                                                {
                                                    elem: 'control',
                                                    content: {
                                                        block: 'social-likes',
                                                        content: [
                                                            {item: 'likes', mix: {block: 'vkontakte'}},
                                                            {item: 'likes', mix: {block: 'facebook'}},
                                                            {item: 'likes', mix: {block: 'plusone'}},
                                                            // {item: 'likes', mix: {block: 'twitter'}},
                                                            {item: 'likes', mix: {block: 'odnoklassniki'}}
                                                        ]
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {lg: 6, xs: 12},
                                    content: [
                                        {
                                            block: 'share',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    // mix: [{block: 'share', elem: 'title'}],
                                                    content: 'Ставь лайки:'
                                                },
                                                {
                                                    elem: 'control',
                                                    content: [
                                                        {
                                                            block: 'soc-likes',
                                                            attrs: {
                                                                id: 'soc-likes'
                                                            }
                                                        },
                                                        {
                                                            block: 'img',
                                                            mix: [{block: 'share', elem: 'vk'}],
                                                            url: 'soc/vk-demo.png'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },

                            ]
                        },
                    ]
                }
            ]
        },
        require('../_common/recently-watched.bemjson.js'),
        
        require('../_common/footer.bemjson.js')
    ]
};
