module.exports = {
    block: 'page',
    title: 'Продукт детально',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../_common/header.bemjson.js')(),
        {
            block: 'block',
            mods: {theme: 'light'},
            attrs: {
                'data-theme': 'light'
            },
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'breadcrumb'
                        },
                        {
                            block: 'title',
                            mods: {first: true},
                            mix: [{block: 'h1'}],
                            tag: 'h1',
                            content: 'Туника Pink Floyd'
                        },
                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '6'},
                                    content: [
                                        {
                                            block: 'slider-detail',
                                            content: [
                                                {
                                                    elem: 'big',
                                                    mods: {detail:true},
                                                    content: [
                                                        {
                                                            elem: 'item',
                                                            mods: {modal: true},
                                                            content: [
                                                                {
                                                                    block: 'img',
                                                                    mix: [{block: 'slider-detail', elem: 'img'}],
                                                                    mods: {lazyload: true, responsive: true},
                                                                    attrs:{
                                                                        'data-zoom-image':'../../../images/product/img-big-1.jpg'
                                                                    },
                                                                    url: 'product/img-big-1.jpg'
                                                                },
                                                                {
                                                                    elem: 'kit',
                                                                    content: 'Товар из комплекта'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'item',
                                                            mods: {modal: true},
                                                            content: [
                                                                {
                                                                    block: 'img',
                                                                    mix: [{block: 'slider-detail', elem: 'img'}],
                                                                    mods: {lazyload: true, responsive: true},
                                                                    attrs:{
                                                                        'data-zoom-image':'../../../images/product/img-big-2.jpg'
                                                                    },
                                                                    url: 'product/img-big-2.jpg'
                                                                },
                                                                {
                                                                    elem: 'kit',
                                                                    content: 'Товар из комплекта'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'item',
                                                            mods: {modal: true},
                                                            content: [
                                                                {
                                                                    block: 'img',
                                                                    mix: [{block: 'slider-detail', elem: 'img'}],
                                                                    mods: {lazyload: true, responsive: true},
                                                                    attrs:{
                                                                        'data-zoom-image':'../../../images/product/img-big-3.jpg'
                                                                    },
                                                                    url: 'product/img-big-3.jpg'
                                                                },
                                                                {
                                                                    elem: 'kit',
                                                                    content: 'Товар из комплекта'
                                                                }
                                                            ]
                                                        },
                                                    ]
                                                },
                                                {
                                                    elem: 'nav',
                                                    content: [
                                                        {
                                                            elem: 'nav-item',
                                                            content: [
                                                                {
                                                                    block: 'img',
                                                                    mods: {responsive: true},
                                                                    url: 'product/img-min-1.jpg'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'nav-item',
                                                            content: [
                                                                {
                                                                    block: 'img',
                                                                    mods: {responsive: true},
                                                                    url: 'product/img-min-2.jpg'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'nav-item',
                                                            content: [
                                                                {
                                                                    block: 'img',
                                                                    mods: {responsive: true},
                                                                    url: 'product/img-min-3.jpg'
                                                                }
                                                            ]
                                                        },
                                                    ]
                                                },
                                                
                                            ]
                                        },

                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '6'},
                                    content: [
                                        {
                                            block: 'product-control',
                                            mods: {detail: true},
                                            attrs: {
                                                id: 'product-control_detail'
                                            },
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Туника Pink Floyd с Мальтийским крестом (Eagle) Обложка для документов "Чикано"'
                                                },
                                                {
                                                    elem: 'id',
                                                    content: [
                                                        {
                                                            elem: 'id-label',
                                                            content: 'Артикул:'
                                                        },
                                                        '01160077'
                                                    ]
                                                },
                                                {
                                                    elem: 'last-in-stock',
                                                    content: 'Последний товар в наличии!'
                                                },
                                                {
                                                    elem: 'row-size',
                                                    content: [
                                                        {
                                                            elem: 'col-size',
                                                            mods: {is: 'one'},
                                                            content: [
                                                                {
                                                                    elem: 'size',
                                                                    content: [
                                                                        {
                                                                            elem: 'size-item',
                                                                            mods: {stock: true},
                                                                            content: 'S'
                                                                        },
                                                                        {
                                                                            elem: 'size-item',
                                                                            mods: {stock: true},
                                                                            content: 'M'
                                                                        },
                                                                        {
                                                                            elem: 'size-item',
                                                                            mods: {stock: true},
                                                                            content: 'L'
                                                                        },
                                                                        {
                                                                            elem: 'size-item',
                                                                            content: 'XL'
                                                                        },
                                                                        {
                                                                            elem: 'size-item',
                                                                            mods: {stock: true},
                                                                            content: 'XXL'
                                                                        },
                                                                    ]
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'col-size',
                                                            mods: {is: 'two'},
                                                            content: [
                                                                {
                                                                    elem: 'size-table',
                                                                    content: [
                                                                        {
                                                                            block: 'size-table-link',
                                                                            attrs: {
                                                                                'data-toggle':'modal',
                                                                                'data-target': '#size-table'
                                                                            },
                                                                            content: 'Таблица размеров'
                                                                        },
                                                                    ]
                                                                },
                                                                {
                                                                    tag: 'a',
                                                                    attrs: {
                                                                        href: '#'
                                                                    },
                                                                    content: 'Бесплатная примерка'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'old-price',
                                                    content: '<s>3 150 ₽</s>'
                                                },
                                                {
                                                    elem: 'price',
                                                    content: [
                                                        '<b>2 990</b> ₽',
                                                        {
                                                            elem: 'bonus',
                                                            content: '<span>Купи и получи</span><br/> 250 баллов!'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'row-to-cart',
                                                    content: [
                                                        {
                                                            elem: 'col-to-cart',
                                                            mods: {for:'btn'},
                                                            content: [
                                                                {
                                                                    block: 'btn',
                                                                    mix: [{block: 'product-control', elem: 'to-cart'}],
                                                                    content: 'Добавить в корзину'
                                                                },
                                                            ]
                                                        },
                                                        {
                                                            elem: 'col-to-cart',
                                                            mods: {for:'label'},
                                                            content: [
                                                                {
                                                                    elem: 'btn-label',
                                                                    content: 'Вы можете вернуть товар без объяснения причин в течении 14 дней'
                                                                }
                                                            ]
                                                        }

                                                    ]
                                                },
                                                {
                                                    elem: 'favorites',
                                                    content: 'В избранное'
                                                }
                                            ]
                                        },
                                        {
                                            block: 'product-shipping',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Доставка, оплата, гарантия'
                                                },
                                                {
                                                    elem: 'table',
                                                    content: [
                                                        {
                                                            elem: 'row',
                                                            content: [
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'label'},
                                                                    content: 'Ваш населенный пункт:'
                                                                },
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'info'},
                                                                    content: [
                                                                        {
                                                                            elem: 'city',
                                                                            content: 'Москва'
                                                                        },
                                                                        {
                                                                            elem: 'another-city',
                                                                            content: 'Выбрать город'
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'row',
                                                            content: [
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'label'},
                                                                    content: 'Способы доставки:'
                                                                },
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'info'},
                                                                    content: 'Самовывоз, Курьер, Почта России'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'row',
                                                            content: [
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'label'},
                                                                    content: 'Доставим уже:'
                                                                },
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'info'},
                                                                    content: 'Завтра'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'row',
                                                            content: [
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'label'},
                                                                    content: 'Стоимость доставки от:'
                                                                },
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'info'},
                                                                    content: '180 р.'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'row',
                                                            content: [
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'label'},
                                                                    content: 'Способы оплаты:'
                                                                },
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'info'},
                                                                    content: 'Наличные курьеру, Банковская карта'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'row',
                                                            content: [
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'label'},
                                                                    content: [
                                                                        {
                                                                            block: 'btn',
                                                                            mods: {color: 'ask'},
                                                                            mix: [{block: 'product-shipping', elem: 'btn'}],
                                                                            content: 'Задать вопрос'
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'info'},
                                                                    mix: [{block: 'hidden-xs'}],
                                                                    content: {
                                                                        elem: 'label-btn',
                                                                        content: '—  Мы на связи, если что-то непонятно'
                                                                    }
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'row',
                                                            content: [
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'label'},
                                                                    content: 'Артикул:'
                                                                },
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'info'},
                                                                    content: '01160077'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'row',
                                                            content: [
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'label'},
                                                                    content: 'Производитель:'
                                                                },
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'info'},
                                                                    content: 'RockWay'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'row',
                                                            content: [
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'label'},
                                                                    content: 'Цвет:'
                                                                },
                                                                {
                                                                    elem: 'col',
                                                                    mods: {for: 'info'},
                                                                    content: 'Черный'
                                                                }
                                                            ]
                                                        },
                                                    ]
                                                },
                                               
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'nav-tabs',
                            content: [
                                {
                                    elem: 'item',
                                    mods: {active: true},
                                    for: 'tab-1',
                                    content: 'Еще с символикой Pink Floyd'
                                },
                                {
                                    elem: 'item',
                                    for: 'tab-2',
                                    content: 'Другие с этим принтом'
                                }
                            ]
                        }
                    ]
                },
                {
                    block: 'tab-content',
                    content: [
                        {
                            elem: 'item',
                            mods: {active: true},
                            id: 'tab-1',
                            content: [
                                {
                                    block: 'container',
                                    content: [
                                        require('../_common/catalog-line.bemjson.js'),
                                        {
                                            block: 'text-center',
                                            content: [
                                                {
                                                    block: 'btn',
                                                    mods: {size: 'sm'},
                                                    content: 'Все товары этой подборки'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            elem: 'item',
                            id: 'tab-2',
                            content: [
                                {
                                    block: 'container',
                                    content: [
                                        require('../_common/catalog-line.bemjson.js'),
                                        {
                                            block: 'text-center',
                                            content: [
                                                {
                                                    block: 'btn',
                                                    mods: {size: 'sm'},
                                                    content: 'Все товары этой подборки'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },

                    ]
                },
                {
                    block: 'container',
                    content: [
                        {
                            block: 'row',
                            mods: {reviews:true},
                            mix: [{ block: 'reviews-block'}],
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '7', lg: '8'},
                                    mix: [{block: 'reviews-block', elem: 'comments'}],
                                    content: [
                                        {
                                            block: 'cackle-me'
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '5', lg: '4'},
                                    mix: [{block: 'reviews-block', elem: 'view'}],
                                    content: [
                                        {
                                            block: 'btn',
                                            mods: {'reviews-main': true, size: 'xs'},
                                            content: 'Оставить отзыв'
                                        },
                                        {
                                            block: 'reviews-item',
                                            content: [
                                                {
                                                    elem: 'text',
                                                    content: 'Изготовленные на заказ (под нужный мне размер и цвет) ботинки сделаны и доставлены быстро. Очень вежливый курьер. изделия - арафатка и 2 банданы, хорошего качества.'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: [
                                                        'Korn',
                                                        {
                                                            elem: 'date',
                                                            content: '09.09.2016'
                                                        },
                                                    ]
                                                },


                                            ]
                                        },
                                        {
                                            block: 'reviews-item',
                                            content: [
                                                {
                                                    elem: 'text',
                                                    content: 'Изготовленные на заказ (под нужный мне размер и цвет) ботинки сделаны и доставлены быстро. Очень вежливый курьер. изделия - арафатка и 2 банданы, хорошего качества.'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: [
                                                        'Спирина С.В.',
                                                        {
                                                            elem: 'date',
                                                            content: '09.09.2016'
                                                        },
                                                    ]
                                                },


                                            ]
                                        },
                                        {
                                            block: 'reviews-item',
                                            content: [
                                                {
                                                    elem: 'text',
                                                    content: 'Изготовленные на заказ (под нужный мне размер и цвет) ботинки сделаны и доставлены быстро. Очень вежливый курьер. изделия - арафатка и 2 банданы, хорошего качества.'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: [
                                                        'Слот',
                                                        {
                                                            elem: 'date',
                                                            content: '09.09.2016'
                                                        },
                                                    ]
                                                },


                                            ]
                                        },
                                        {
                                            block: 'a',
                                            mix: [{block: 'mbl'}],
                                            content: 'Читай все отзывы'
                                        }

                                    ]
                                }
                            ]
                        }

                    ]
                }
            ]
        },
        require('../_common/recently-watched.bemjson.js'),
        require('../_common/footer.bemjson.js')
    ]
};
