module.exports = {
    block: 'page',
    title: 'Акции',
    styles: [{elem: 'css', url: '../_merged/_merged.css'},
        {
            elem: 'css',
            url: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;subset=cyrillic-ext'
        }
    ],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../_common/header.bemjson.js')(),

        {
            block: 'block',
            mods: {theme: 'light'},
            attrs: {
                'data-theme': 'light'
            },
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'breadcrumb'
                        },
                        {
                            block: 'title',
                            mods: {first:true},
                            mix: [{block: 'h1'}],
                            tag: 'h1',
                            content: 'Рок атрибутика и аксессуары'
                        },
                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '3', sm: '4'},
                                    content: [
                                        {
                                            block: 'sidebar',
                                            content: [
                                                {
                                                    block: 'sidebar-menu',
                                                    content: [
                                                        'Галстуки',
                                                        'Зеркала',
                                                        'Напульсники и фенечки',
                                                        'Нашивки',
                                                        'Маски',
                                                        'Ошейники',
                                                        'Ремни',
                                                        'Пряжки',
                                                        'Тату-рукава',
                                                        'Хвосты',
                                                        'Цепи и карабины',
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '9', sm: '8'},
                                    content: [
                                        {
                                            block: 'stock-list',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Косуха First легкая Special за 9900!'
                                                },
                                                {
                                                    elem: 'img',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true, responsive: true},
                                                            url: 'catalog/img-big-1.jpg'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'text',
                                                    content: [
                                                        'Аттракцион неслыханной щедрости продолжается! <br/> Да, да, Друзья! Наступил последний месяц зимы, а мы решили повторить новогоднюю акцию! Все товары с символикой Castle Rock - в половину стоимости! <be/>На этот раз, акция может быть завершена в любой момент. Поторопись! <br/> Список товаров со скидкой в интернет магазине <br/>Обратите внимание, что на сайте представлен не полный список товаров. С полным ассортиментом можно ознакомиться по адресу Лиговский пр, 47'
                                                    ]
                                                },
                                                {
                                                    elem: 'more',
                                                    content: 'Подробнее'
                                                }
                                            ]
                                        },
                                        {
                                            block: 'stock-list',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Акция "50% скидка на товары с символикой Castle Rock (продление)" (Акция действует ТОЛЬКО в магазине на Лиговском, 47)'
                                                },
                                                {
                                                    elem: 'img',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true, responsive: true},
                                                            web: true,
                                                            url: 'http://dev.castlerock.ru/upload/medialibrary/e34/e34e7fc9bcaa0e967aea771cf55a409d.jpg'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'text',
                                                    content: [
                                                        'Аттракцион неслыханной щедрости продолжается! <br/> Да, да, Друзья! Наступил последний месяц зимы, а мы решили повторить новогоднюю акцию! Все товары с символикой Castle Rock - в половину стоимости! <be/>На этот раз, акция может быть завершена в любой момент. Поторопись! <br/> Список товаров со скидкой в интернет магазине <br/>Обратите внимание, что на сайте представлен не полный список товаров. С полным ассортиментом можно ознакомиться по адресу Лиговский пр, 47'
                                                    ]
                                                },
                                                {
                                                    elem: 'more',
                                                    content: 'Подробнее'
                                                }
                                            ]
                                        },
                                        {
                                            block: 'stock-list',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Акция «Рок - Почтой» (Акция закончена)'
                                                },
                                                {
                                                    elem: 'img',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true, responsive: true},
                                                            web: true,
                                                            url: 'http://dev.castlerock.ru/upload/resize_cache/iblock/d6f/200_400_1/d6f10ba7bf8581d437e8b8cea0cb1ce8.png'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'text',
                                                    content: [
                                                        'Аттракцион неслыханной щедрости продолжается! <br/> Да, да, Друзья! Наступил последний месяц зимы, а мы решили повторить новогоднюю акцию! Все товары с символикой Castle Rock - в половину стоимости! <be/>На этот раз, акция может быть завершена в любой момент. Поторопись! <br/> Список товаров со скидкой в интернет магазине <br/>Обратите внимание, что на сайте представлен не полный список товаров. С полным ассортиментом можно ознакомиться по адресу Лиговский пр, 47'
                                                    ]
                                                },
                                                {
                                                    elem: 'more',
                                                    content: 'Подробнее'
                                                }
                                            ]
                                        },
                                        {
                                            block: 'stock-list',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Косуха First легкая Special за 9900!'
                                                },
                                                {
                                                    elem: 'img',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true, responsive: true},
                                                            url: 'catalog/img-big-1.jpg'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'text',
                                                    content: [
                                                        'Аттракцион неслыханной щедрости продолжается! <br/> Да, да, Друзья! Наступил последний месяц зимы, а мы решили повторить новогоднюю акцию! Все товары с символикой Castle Rock - в половину стоимости! <be/>На этот раз, акция может быть завершена в любой момент. Поторопись! <br/> Список товаров со скидкой в интернет магазине <br/>Обратите внимание, что на сайте представлен не полный список товаров. С полным ассортиментом можно ознакомиться по адресу Лиговский пр, 47'
                                                    ]
                                                },
                                                {
                                                    elem: 'more',
                                                    content: 'Подробнее'
                                                }
                                            ]
                                        },



                                    ]
                                }
                            ]
                        },
                    ]
                }
            ]
        },
        require('../_common/recently-watched.bemjson.js'),

        require('../_common/footer.bemjson.js')
    ]
};
