module.exports = {
    block: 'page',
    title: 'Главная',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../_common/header.bemjson.js')(),
        {
            block: 'slider-main',
            attrs:{
                'data-speed': 4000
            },
            content: [
                {
                    elem: 'item',
                    content: [
                        {
                            block: 'img',
                            mix: [{block: 'slider-main', elem: 'img'}],
                            url: 'slider-main/img-2.jpg'
                        },
                        {
                            elem: 'title',
                            content: 'Slipknot'
                        },
                        {
                            elem: 'btn-wrap',
                            content: 'Специальная подборка к дню рождения'
                        }
                    ]
                },
                {
                    elem: 'item',
                    content: [
                        {
                            block: 'img',
                            mix: [{block: 'slider-main', elem: 'img'}],
                            url: 'slider-main/img-2.jpg'
                        },
                        {
                            elem: 'title',
                            content: 'Slipknot'
                        },
                        {
                            elem: 'btn-wrap',
                            content: 'Специальная подборка к дню рождения'
                        }
                    ]
                },
                {
                    elem: 'item',
                    content: [
                        {
                            block: 'img',
                            mix: [{block: 'slider-main', elem: 'img'}],
                            url: 'slider-main/img-2.jpg'
                        },
                        {
                            elem: 'title',
                            content: 'Slipknot'
                        },
                        {
                            elem: 'btn-wrap',
                            content: 'Специальная подборка к дню рождения'
                        }
                    ]
                },
            ]
        },
        {
            block: 'block',
            mods: {theme: 'light', for: 'slider'},
            attrs: {
                'data-theme': 'light'
            },
            content: [
                {
                    elem: 'shadow-top',
                },
                {
                    block: 'container',
                    content: [
                        {
                            block: 'title',
                            mods: {main: true},
                            content: [
                                'Популярные товары',
                                {
                                    elem: 'sub',
                                    content: 'Все товары в наличии! Отправка в день заказа!'
                                },
                            ]
                        },
                        {
                            block: 'row',
                            mods: {card: true},
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '3', xs: '6'},
                                    content: [
                                        {
                                            block: 'card',
                                            mods: {main: true},
                                            new: true,
                                            img: 'card/card-5-1.jpg',
                                            img2: 'card/card-5-2.jpg',
                                            bonus: '+ 199 баллов',
                                            title: 'Майка Black Veil Brides',
                                            price: '790'
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '3', xs: '6'},
                                    content: [
                                        {
                                            block: 'card',
                                            mods: {main: true},
                                            img: 'card/card-3-1.jpg',
                                            img2: 'card/card-3-2.jpg',
                                            title: 'Жилет кожаный First женский'
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '3', xs: '6'},
                                    mix: [{block: 'hidden-xs'}],
                                    content: [
                                        {
                                            block: 'card',
                                            mods: {main: true},
                                            new: true,
                                            img: 'card/card-2-1.jpg',
                                            img2: 'card/card-2-2.jpg',
                                            title: 'Жилет кожаный First женский lack Veil Brides Брюки в клетку Красная крупная'
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '3', xs: '6'},
                                    mix: [{block: 'hidden-xs'}],
                                    content: [
                                        {
                                            block: 'card',
                                            mods: {main: true},
                                            bonus: '+ 199 баллов',
                                        }
                                    ]
                                },

                            ]
                        },
                        {
                            block: 'title',
                            mods: {main: true},
                            content: [
                                'Новинки',
                                {
                                    elem: 'sub',
                                    content: 'Отслеживаем всё самое новое в рок-атрибутике'
                                }
                            ]
                        },

                        {
                            block: 'row',
                            mods: {card: true},
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '3', xs: '6'},
                                    content: [
                                        {
                                            block: 'card',
                                            mods: {main: true},
                                            img: 'card/card-4-1.jpg',
                                            img2: 'card/card-4-2.jpg',
                                            bonus: '+ 199 баллов'
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '3', xs: '6'},
                                    content: [
                                        {
                                            block: 'card',
                                            mods: {main: true},
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '3', xs: '6'},
                                    mix: [{block: 'hidden-xs'}],
                                    content: [
                                        {
                                            block: 'card',
                                            mods: {main: true},
                                            img: 'card/card-5-1.jpg',
                                            img2: 'card/card-5-2.jpg',
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '3', xs: '6'},
                                    mix: [{block: 'hidden-xs'}],
                                    content: [
                                        {
                                            block: 'card',
                                            mods: {main: true},
                                        }
                                    ]
                                },

                            ]
                        }
                    ]
                },
            ]
        },
        {
            block: 'block',
            mods: {theme: 'dark-wallpapers'},
            mix: [{block: 'hidden-xs'}],
            attrs: {
                'data-theme': 'dark'
            },
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'collection',
                            content: [
                                {
                                    block: 'title',
                                    mods: {thin: true},
                                    content: [
                                        {
                                            elem: 'span',
                                            content: 'Подборки товаров'
                                        },
                                        {
                                            block: 'btn',
                                            mix: [{block: 'collection', elem: 'btn'}],
                                            content: 'Смотреть все подборки'
                                        }
                                    ]
                                },
                                {
                                    block: 'row',
                                    content: [
                                        {
                                            elem: 'col',
                                            mods: {lg: '8', sm: '12'},
                                            content: [
                                                {
                                                    block: 'collection-list',
                                                    mods: {'column': '4'},
                                                    title: 'Зарубежные группы',
                                                    content: [
                                                        "30 Seconds To Mars",
                                                        "A Day To Remember",
                                                        "AC/DC",
                                                        "Accept",
                                                        "Aerosmith",
                                                        "Amon Amarth",
                                                        "Anthrax",
                                                        "Arch Enemy",
                                                        "Arctic Monkeys",
                                                        "Asking Alexandria",
                                                        "Avenged Sevenfold",
                                                        "Beatles",
                                                        "Behemoth",
                                                        "Black Sabbath",
                                                        "Black Veil Brides",
                                                        "Blink-182",
                                                        "Bob Marley",
                                                        "Bring Me The Horizon",
                                                        "Bullet For My Valentine",
                                                        "Burzum",
                                                        "Cannibal Corpse",
                                                        "Children Of Bodom",
                                                        "Cradle Of Filth",
                                                        "Death",
                                                        "Deep Purple",
                                                        "Depeche Mode",
                                                        "Dimmu Borgir",
                                                        "Disturbed",
                                                        "Doors",
                                                        "Dream Theater",
                                                        "Exploited",
                                                        "Green Day",
                                                        "Guns'N'Roses",
                                                        "Him",
                                                        "Hollywood Undead",
                                                        "In Flames",
                                                        "Iron Maiden",
                                                        "Judas Priest",
                                                        "Kiss",
                                                        "Korn",
                                                        "Lacrimosa",
                                                        "Lamb of God",
                                                        "Led Zeppelin",
                                                        "Linkin Park",
                                                        "Manowar",
                                                        "Marilyn Manson",
                                                        "Megadeth",
                                                        "Metallica",
                                                        "Misfits",
                                                        "Motorhead",
                                                        "Muse",
                                                        "My Chemical Romance",
                                                        "Nightwish",
                                                        "Nirvana",
                                                        "Ozzy Osbourne",
                                                        "Pantera",
                                                        "Pink Floyd",
                                                        "Rammstein",
                                                        "Ramones",
                                                        "Red Hot Chili Peppers",
                                                        "Rolling Stones",
                                                        "Sex Pistols",
                                                        "Skillet",
                                                        "Slayer",
                                                        "Slipknot",
                                                        "Suicide Silence",
                                                        "System Of A Down",
                                                        "Three Days Grace",
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'col',
                                            mods: {lg: '2', sm: '6'},
                                            content: [
                                                {
                                                    block: 'collection-list',
                                                    title: 'Российские группы',
                                                    content: [
                                                        "Amatory",
                                                        "Ария",
                                                        "Гражданская оборона",
                                                        "Алиса",
                                                        "Король И Шут",
                                                        "Пилот",
                                                        "Кино",
                                                        "Сектор Газа",
                                                        "Louna",
                                                        "КняZz",
                                                        "Пикник",
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'col',
                                            mods: {lg: '2', sm: '6'},
                                            content: [
                                                {
                                                    block: 'collection-list',
                                                    title: 'Тематика',
                                                    content: [
                                                        "Волки",
                                                        "Драконы",
                                                        "Черепа",
                                                        "Викинги",
                                                        "Милитари",
                                                        "Комиксы",
                                                        "14 февраля",
                                                        "23 февраля",
                                                        "8 марта",
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                            ]
                        },
                        {
                            block: 'title',
                            mods: {thin: true},
                            content: 'Звезды закупаются у нас!'
                        },
                        {
                            block: 'slider-reviews',
                            content: [
                                {
                                    elem: 'item',
                                    content: [
                                        {
                                            block: 'img',
                                            mix: [{block: 'slider-reviews', elem: 'img'}],
                                            url: 'slider-reviews/img-1.jpg'
                                        },
                                        {
                                            elem: 'info',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'NIGHTWISH'
                                                },
                                                {
                                                    elem: 'text',
                                                    content: 'Изготовленные на заказ (под нужный мне размер и цвет) ботинки сделаны и доставлены быстро. Качество радует, очень хорошо смотрятся. Спасибо большое! Запомню вас'
                                                },
                                                {
                                                    elem: 'more',
                                                    content: 'Подробнее'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'item',
                                    content: [
                                        {
                                            block: 'img',
                                            mix: [{block: 'slider-reviews', elem: 'img'}],
                                            url: 'slider-reviews/img-2.jpg'
                                        },
                                        {
                                            elem: 'info',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Слот'
                                                },
                                                {
                                                    elem: 'text',
                                                    content: 'Изготовленные на заказ (под нужный мне размер и цвет) ботинки сделаны и доставлены быстро. Качество радует, очень хорошо смотрятся. Спасибо большое! Запомню вас'
                                                },
                                                {
                                                    elem: 'more',
                                                    content: 'Подробнее'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'item',
                                    content: [
                                        {
                                            block: 'img',
                                            mix: [{block: 'slider-reviews', elem: 'img'}],
                                            url: 'slider-reviews/img-3.jpg'
                                        },
                                        {
                                            elem: 'info',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Napalm Death'
                                                },
                                                {
                                                    elem: 'text',
                                                    content: 'Изготовленные на заказ (под нужный мне размер и цвет) ботинки сделаны и доставлены быстро. Качество радует, очень хорошо смотрятся. Спасибо большое! Запомню вас'
                                                },
                                                {
                                                    elem: 'more',
                                                    content: 'Подробнее'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'item',
                                    content: [
                                        {
                                            block: 'img',
                                            mix: [{block: 'slider-reviews', elem: 'img'}],
                                            url: 'slider-reviews/img-1.jpg'
                                        },
                                        {
                                            elem: 'info',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Слот'
                                                },
                                                {
                                                    elem: 'text',
                                                    content: 'Изготовленные на заказ (под нужный мне размер и цвет) ботинки сделаны и доставлены быстро. Качество радует, очень хорошо смотрятся. Спасибо большое! Запомню вас'
                                                },
                                                {
                                                    elem: 'more',
                                                    content: 'Подробнее'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'item',
                                    content: [
                                        {
                                            block: 'img',
                                            mix: [{block: 'slider-reviews', elem: 'img'}],
                                            url: 'slider-reviews/img-2.jpg'
                                        },
                                        {
                                            elem: 'info',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Слот'
                                                },
                                                {
                                                    elem: 'text',
                                                    content: 'Изготовленные на заказ (под нужный мне размер и цвет) ботинки сделаны и доставлены быстро. Качество радует, очень хорошо смотрятся. Спасибо большое! Запомню вас'
                                                },
                                                {
                                                    elem: 'more',
                                                    content: 'Подробнее'
                                                }
                                            ]
                                        }
                                    ]
                                },
                            ]
                        }
                    ]
                },
            ]
        },
        {
            block: 'block',
            mods: {theme: 'dark', for: 'reviews'},
            attrs: {
                'data-theme': 'dark'
            },
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '9'},
                                    content: [
                                        {
                                            block: 'article-main',
                                            content: [
                                                {
                                                    block: 'title',
                                                    mods: {reviews: true},
                                                    content: 'Castle Rock – твой интернет <br/>магазин рок атрибутики'
                                                },
                                                {
                                                    tag: 'p',
                                                    content: 'Можно ли купить рок? Ни за что. Рок не продается. Он всегда был и останется самым независимым направлением музыки. Бунтарство и протест деньгами не измерить. Но можно купить то, что с роком связано, напрямую или опосредованно, через различные предметы и вещи с рок-символикой. И делать это лучше всего в особом рок-магазине. Вот он, прямо перед тобой, смотри – ты уже в нем! Добро пожаловать в интернет-магазин рок-атрибутики Castle Rock!'
                                                },
                                                {
                                                    tag: 'p',
                                                    content: 'Это самый неформальный магазин из всех. Castle Rock – это легенда, культовый магазин для рокеров уже больше двадцати лет. Старейший в России и крупнейший в Европе, место паломничества и общения единомышленников. У нас огромный ассортимент – более 20 000 наименований рок-атрибутики и неформальной одежды: косухи, кожаные плащи и штаны, пряжки, уникальные нашивки, банданы, напульсники и ремни с заклепками и шипами, тысячи видов футболок и толстовок с любимыми рок-группами, гады, берцы, казаки, готические платья и корсеты, множество перстней и подвесов, серьги и косметички, книги и журналы, и даже товары для детей. И конечно, рок-музыка. Но главное в Castle Rock – это единый, уникальный рок стиль, присущий каждой вещи, которая здесь продается. С пустыми руками от нас не уходят, каждый найдет себе товар по душе.'
                                                },
                                                {
                                                    block: 'collapse',
                                                    attrs: {
                                                        id: 'article-main-collapse'
                                                    },
                                                    content: [
                                                        {
                                                            tag: 'p',
                                                            content: 'Это самый неформальный магазин из всех. Castle Rock – это легенда, культовый магазин для рокеров уже больше двадцати лет. Старейший в России и крупнейший в Европе, место паломничества и общения единомышленников. У нас огромный ассортимент – более 20 000 наименований рок-атрибутики и неформальной одежды: косухи, кожаные плащи и штаны, пряжки, уникальные нашивки, банданы, напульсники и ремни с заклепками и шипами, тысячи видов футболок и толстовок с любимыми рок-группами, гады, берцы, казаки, готические платья и корсеты, множество перстней и подвесов, серьги и косметички, книги и журналы, и даже товары для детей. И конечно, рок-музыка. Но главное в Castle Rock – это единый, уникальный рок стиль, присущий каждой вещи, которая здесь продается. С пустыми руками от нас не уходят, каждый найдет себе товар по душе.'
                                                        },
                                                        {
                                                            tag: 'p',
                                                            content: 'Можно ли купить рок? Ни за что. Рок не продается. Он всегда был и останется самым независимым направлением музыки. Бунтарство и протест деньгами не измерить. Но можно купить то, что с роком связано, напрямую или опосредованно, через различные предметы и вещи с рок-символикой. И делать это лучше всего в особом рок-магазине. Вот он, прямо перед тобой, смотри – ты уже в нем! Добро пожаловать в интернет-магазин рок-атрибутики Castle Rock!'
                                                        },
                                                    ]
                                                },
                                                {
                                                    block: 'readmore-link',
                                                    attrs: {
                                                        'data-toggle': 'collapse',
                                                        'data-target': '#article-main-collapse',
                                                        'data-text': 'Свернуть'
                                                    },
                                                    content: 'Читать далее'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '3'},
                                    content: [
                                        {
                                            block: 'btn',
                                            mods: {'reviews-main': true},
                                            content: 'Оставить отзыв'
                                        },
                                        {
                                            block: 'reviews-item',
                                            content: [
                                                {
                                                    elem: 'text',
                                                    content: 'Изготовленные на заказ (под нужный мне размер и цвет) ботинки сделаны и доставлены быстро. Очень вежливый курьер. изделия - арафатка и 2 банданы, хорошего качества.'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: [
                                                        'Korn',
                                                        {
                                                            elem: 'date',
                                                            content: '09.09.2016'
                                                        },
                                                    ]
                                                },


                                            ]
                                        },
                                        {
                                            block: 'reviews-item',
                                            content: [
                                                {
                                                    elem: 'text',
                                                    content: 'Изготовленные на заказ (под нужный мне размер и цвет) ботинки сделаны и доставлены быстро. Очень вежливый курьер. изделия - арафатка и 2 банданы, хорошего качества.'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: [
                                                        'Спирина С.В.',
                                                        {
                                                            elem: 'date',
                                                            content: '09.09.2016'
                                                        },
                                                    ]
                                                },


                                            ]
                                        },
                                        {
                                            block: 'reviews-item',
                                            content: [
                                                {
                                                    elem: 'text',
                                                    content: 'Изготовленные на заказ (под нужный мне размер и цвет) ботинки сделаны и доставлены быстро. Очень вежливый курьер. изделия - арафатка и 2 банданы, хорошего качества.'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: [
                                                        'Слот',
                                                        {
                                                            elem: 'date',
                                                            content: '09.09.2016'
                                                        },
                                                    ]
                                                },


                                            ]
                                        },
                                        {
                                            block: 'a',
                                            content: 'Читай все отзывы'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }


            ]
        },
        {
            block: 'block',
            mods: {theme: 'blog'},
            attrs: {
                'data-theme': 'dark'
            },
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'title-link',
                            content: 'Читай Рок блог'
                        },
                        {
                            block: 'slider-blog',
                            content: [
                                {
                                    elem: 'item',
                                    content: [
                                        {
                                            block: 'blog-preview',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mix: [{block: 'slider-reviews', elem: 'img'}],
                                                    url: 'slider-reviews/img-1.jpg'
                                                },
                                                {
                                                    elem: 'info',
                                                    content: [
                                                        {
                                                            elem: 'date',
                                                            content: '21.09.2016'
                                                        },
                                                        {
                                                            elem: 'title',
                                                            content: 'Vader 09.09.16 Opera Concert Club'
                                                        },
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'item',
                                    content: [
                                        {
                                            block: 'blog-preview',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mix: [{block: 'slider-reviews', elem: 'img'}],
                                                    url: 'slider-reviews/img-2.jpg'
                                                },
                                                {
                                                    elem: 'info',
                                                    content: [
                                                        {
                                                            elem: 'date',
                                                            content: '21.09.2016'
                                                        },
                                                        {
                                                            elem: 'title',
                                                            content: 'Песни в память о теракте 11 сентября 2001 года...'
                                                        },
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'item',
                                    content: [
                                        {
                                            block: 'blog-preview',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mix: [{block: 'slider-reviews', elem: 'img'}],
                                                    url: 'slider-reviews/img-3.jpg'
                                                },
                                                {
                                                    elem: 'info',
                                                    content: [
                                                        {
                                                            elem: 'date',
                                                            content: '21.09.2016'
                                                        },
                                                        {
                                                            elem: 'title',
                                                            content: 'Юбилей Михаила Семёнова "Декабрь"'
                                                        },
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'item',
                                    content: [
                                        {
                                            block: 'blog-preview',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mix: [{block: 'slider-reviews', elem: 'img'}],
                                                    url: 'slider-reviews/img-3.jpg'
                                                },
                                                {
                                                    elem: 'info',
                                                    content: [
                                                        {
                                                            elem: 'date',
                                                            content: '21.09.2016'
                                                        },
                                                        {
                                                            elem: 'title',
                                                            content: 'Юбилей Михаила Семёнова "Декабрь"'
                                                        },
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                            ]
                        },
                    ]
                }
            ]
        },
        {
            block: 'block',
            mods: {theme: 'dark', for: 'share'},
            attrs: {
                'data-theme': 'dark'
            },
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    mods: {lg: 6, xs: 12},
                                    content: [
                                        {
                                            block: 'share',
                                            mods: {right: true},
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Делись:'
                                                },
                                                {
                                                    elem: 'control',
                                                    content: {
                                                        block: 'social-likes',
                                                        content: [
                                                            {item: 'likes', mix: {block: 'vkontakte'}},
                                                            {item: 'likes', mix: {block: 'facebook'}},
                                                            {item: 'likes', mix: {block: 'plusone'}},
                                                            // {item: 'likes', mix: {block: 'twitter'}},
                                                            {item: 'likes', mix: {block: 'odnoklassniki'}}
                                                        ]
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {lg: 6, xs: 12},
                                    content: [
                                        {
                                            block: 'share',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    // mix: [{block: 'share', elem: 'title'}],
                                                    content: 'Ставь лайки:'
                                                },
                                                {
                                                    elem: 'control',
                                                    content: [
                                                        {
                                                            block: 'soc-likes',
                                                            attrs: {
                                                                id: 'soc-likes'
                                                            }
                                                        },
                                                        {
                                                            block: 'img',
                                                            mix: [{block: 'share', elem: 'vk'}],
                                                            url: 'soc/vk-demo.png'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },

                            ]
                        },

                    ]
                }
            ]
        },

        require('../_common/footer.bemjson.js'),
        require('../_common/quick-view.bemjson.js')
    ]
};
