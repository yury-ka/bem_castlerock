module.exports = {
    block: 'page',
    title: 'Список галерея',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../_common/header.bemjson.js')(),

        {
            block: 'block',
            mods: {theme: 'light'},
            attrs: {
                'data-theme': 'light'
            },
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'breadcrumb'
                        },
                        {
                            block: 'title',
                            mods: {first:true},
                            mix: [{block: 'h1'}],
                            tag: 'h1',
                            content: 'Рок атрибутика и аксессуары'
                        },
                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    mods: {xs: '12'},
                                    content: [
                                        {
                                            block: 'article',
                                            mods: {'blog-list': true},
                                            content: [
                                                {
                                                    elem: 'img',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true, responsive: true},
                                                            url: 'slider-reviews/img-1.jpg'
                                                        }
                                                    ]
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Благодаря организаторам концерта - Rain Embraсe Concert Аgency нам удалось попасть на концертную площадку задолго до открытия входных дверей, и своими глазами увидеть эксклюзивную вещь - саундчек PAIN. Мы сразу обратили внимание, что сам маэстро - Питер Тагтгрен, принимает активное участие в отстройке звука на всех этапах. Он неоднократно подходил к звукорежиссеру, давая ценные указания. Стало понятно, что отличный звук на сегодняшнем концерте нам гарантирован. '
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Первой выступала разогревающая питерская группа PitchBlack. Ребята очень старались, хотя их музыкальный стиль немного выбивался из формата мероприятия. '
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Заиграла интро-фонограмма (Billy Idol – Rebel Yell), а значит пришло время для хэдлайнера этого вечера, и ровно в девять на сцене появились PAIN. Питер Тагтгрен в своём классическом концертном одеянии – смирительной рубашке с длинными рукавами, за барабанной установкой его сын – Себастиан Тагтгрен (записывал партии ударных к новому альбому PAIN “Coming Home”, который вышел в сентябре этого года), на гитаре – Грегер Андерсон. В связи с личными семейными проблемами бас-гитарист Андре Скауг не смог присоединиться к группе (пропустил оба российских выступления) – его заменял гитарный техник Nightwish/PAIN Анти Тойвиайнен.'
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Благодаря организаторам концерта - Rain Embraсe Concert Аgency нам удалось попасть на концертную площадку задолго до открытия входных дверей, и своими глазами увидеть эксклюзивную вещь - саундчек PAIN. Мы сразу обратили внимание, что сам маэстро - Питер Тагтгрен, принимает активное участие в отстройке звука на всех этапах. Он неоднократно подходил к звукорежиссеру, давая ценные указания. Стало понятно, что отличный звук на сегодняшнем концерте нам гарантирован. '
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Первой выступала разогревающая питерская группа PitchBlack. Ребята очень старались, хотя их музыкальный стиль немного выбивался из формата мероприятия. Заиграла интро-фонограмма (Billy Idol – Rebel Yell), а значит пришло время для хэдлайнера этого вечера, и ровно в девять на сцене появились PAIN. Питер Тагтгрен в своём классическом концертном одеянии – смирительной рубашке с длинными рукавами, за барабанной установкой его сын – Себастиан Тагтгрен (записывал партии ударных к новому альбому PAIN “Coming Home”, который вышел в сентябре этого года), на гитаре – Грегер Андерсон. В связи с личными семейными проблемами бас-гитарист Андре Скауг не смог присоединиться к группе (пропустил оба российских выступления) – его заменял гитарный техник Nightwish/PAIN Анти Тойвиайнен. '
                                                },
                                            ]
                                        },
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6'},
                                    content: [
                                        {
                                            block: 'blog-list-item',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mix: [{block: 'blog-list-item', elem: 'img'}],
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-reviews/img-1.jpg'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: 'NIGHTWISH'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6'},
                                    content: [
                                        {
                                            block: 'blog-list-item',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mix: [{block: 'blog-list-item', elem: 'img'}],
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-reviews/img-2.jpg'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: 'Слот'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6'},
                                    content: [
                                        {
                                            block: 'blog-list-item',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mix: [{block: 'blog-list-item', elem: 'img'}],
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-reviews/img-3.jpg'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: 'Napalm Death'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6'},
                                    content: [
                                        {
                                            block: 'blog-list-item',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mix: [{block: 'blog-list-item', elem: 'img'}],
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-reviews/img-1.jpg'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: 'NIGHTWISH'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6'},
                                    content: [
                                        {
                                            block: 'blog-list-item',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mix: [{block: 'blog-list-item', elem: 'img'}],
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-reviews/img-2.jpg'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: 'Слот'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6'},
                                    content: [
                                        {
                                            block: 'blog-list-item',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mix: [{block: 'blog-list-item', elem: 'img'}],
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-reviews/img-3.jpg'
                                                },
                                                {
                                                    elem: 'title',
                                                    content: 'Napalm Death'
                                                }
                                            ]
                                        }
                                    ]
                                },
                            ]
                        },
                        {
                            block: 'row',
                            mix: [{block: 'mbl'}],
                            content: [
                                {
                                    elem: 'col',
                                    mods: {lg: 6, xs: 12},
                                    content: [
                                        {
                                            block: 'share',
                                            mods: {right: true},
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Делись:'
                                                },
                                                {
                                                    elem: 'control',
                                                    content: {
                                                        block: 'social-likes',
                                                        content: [
                                                            {item: 'likes', mix: {block: 'vkontakte'}},
                                                            {item: 'likes', mix: {block: 'facebook'}},
                                                            {item: 'likes', mix: {block: 'plusone'}},
                                                            // {item: 'likes', mix: {block: 'twitter'}},
                                                            {item: 'likes', mix: {block: 'odnoklassniki'}}
                                                        ]
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {lg: 6, xs: 12},
                                    content: [
                                        {
                                            block: 'share',
                                            content: [
                                                {
                                                    elem: 'title',
                                                    // mix: [{block: 'share', elem: 'title'}],
                                                    content: 'Ставь лайки:'
                                                },
                                                {
                                                    elem: 'control',
                                                    content: [
                                                        {
                                                            block: 'soc-likes',
                                                            attrs: {
                                                                id: 'soc-likes'
                                                            }
                                                        },
                                                        {
                                                            block: 'img',
                                                            mix: [{block: 'share', elem: 'vk'}],
                                                            url: 'soc/vk-demo.png'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },

                            ]
                        },
                    ]
                }
            ]
        },
        require('../_common/recently-watched.bemjson.js'),
        
        require('../_common/footer.bemjson.js')
    ]
};
