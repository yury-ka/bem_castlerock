module.exports = [
    {
        block: 'selection-block',
        content: [
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'A'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'A'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'B'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'B'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'C'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'B'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'D'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'A'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'E'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'B'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'F'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'B'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'G'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'A'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'I'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'B'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'J'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'B'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'K'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'A'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'L'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'B'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'M'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'B'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'N'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'A'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'O'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'B'
                    }
                ]
            },
            {
                elem: 'row-letter',
                content: [
                    {
                        elem: 'title-list',
                        content: 'P'
                    },
                    {
                        elem: 'letter-list',
                        variant: 'B'
                    }
                ]
            },
            {
                elem: 'vertical-letter',
                content: [
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'A'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'B'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'B'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'C'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'D'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'B'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'E'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'F'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'B'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'G'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'H'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'B'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'I'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'J'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'B'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'K'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'L'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'B'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'M'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'N'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'B'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'O'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'P'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'Q'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'B'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'R'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'S'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'B'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'T'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'U'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'V'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'W'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'X'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'Y'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'B'
                            }
                        ]
                    },
                    {
                        elem: 'vertical-item',
                        content: [
                            {
                                elem: 'vertical-title',
                                content: 'X'
                            },
                            {
                                elem: 'vertical-menu',
                                variant: 'A'
                            }
                        ]
                    },

                ]
            }
        ]
    },
]