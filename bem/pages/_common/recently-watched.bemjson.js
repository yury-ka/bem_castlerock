module.exports = [
    {
        block: 'block',
        mods: {theme: 'dark', for: 'recently'},
        attrs: {
            'data-theme': 'dark'
        },
        content: [
            {
                block: 'container',
                content: [
                    {
                        block: 'title',
                        mods: {recently: true},
                        mix: [{block: 'h3'}],
                        content: 'Вы недавно смотрели:'
                    },
                    {
                        block: 'row',
                        mods: {card: true, catalog: true},
                        content: [
                            {
                                elem: 'col',
                                mods: {lg: '2', sm: '3', xs: '6'},
                                content: [
                                    {
                                        block: 'card',
                                        mods: {'recently': true},
                                        new: true,
                                        img: 'card/card-2-1.jpg',
                                        img2: 'card/card-2-2.jpg',
                                        bonus: '+ 199 баллов',
                                        title: 'Майка Black Veil Brides',
                                        price: '790'
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {lg: '2', sm: '3', xs: '6'},
                                content: [
                                    {
                                        block: 'card',
                                        img: 'card/card-3-1.jpg',
                                        img2: 'card/card-3-2.jpg',
                                        mods: {'recently': true},
                                        title: 'Жилет кожаный First женский'
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {lg: '2', sm: '3', xs: '6'},
                                content: [
                                    {
                                        block: 'card',
                                        img: 'card/card-2-1.jpg',
                                        img2: 'card/card-2-2.jpg',
                                        mods: {'recently': true},
                                        title: 'Жилет кожаный First женский lack Veil Brides Брюки в клетку Красная крупная'
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {lg: '2', sm: '3', xs: '6'},
                                mix: [{block: 'hidden-xs'}, {block: 'hidden-sm'}, {block: 'hidden-md'}],
                                content: [
                                    {
                                        block: 'card',
                                        img: 'card/card-2-1.jpg',
                                        img2: 'card/card-2-2.jpg',
                                        mods: {'recently': true},
                                        bonus: '+ 199 баллов',
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {lg: '2', sm: '3', xs: '6'},
                                content: [
                                    {
                                        block: 'card',
                                        mods: {'recently': true},
                                        bonus: '+ 199 баллов',
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {lg: '2', sm: '3', xs: '6'},
                                mix: [{block: 'hidden-xs'}, {block: 'hidden-sm'}, {block: 'hidden-md'}],
                                content: [
                                    {
                                        block: 'card',
                                        img: 'card/card-3-1.jpg',
                                        img2: 'card/card-3-2.jpg',
                                        mods: {'recently': true},
                                        bonus: '+ 199 баллов',
                                    }
                                ]
                            },
                        ]
                    },
                ]
            },


        ]
    },
]