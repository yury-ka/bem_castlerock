module.exports = [
    {block: 'breadcrumb'},
    {tag: 'h1', content: 'Заголовок h1 — основной заголовок страницы на две строки. Основной заголовок страницы.'},
    {
        block: 'row',
        content: [
            {
                elem: 'col',
                mods: {sm: '3'},
                content: [
                    {
                        block: 'sidebar',
                        content: [
                            {
                                block: 'sidebar-menu',
                                content: [
                                    'Галстуки',
                                    'Зеркала',
                                    'Напульсники и фенечки',
                                    'Нашивки',
                                    'Маски',
                                    'Ошейники',
                                    'Ремни',
                                    'Пряжки',
                                    'Тату-рукава',
                                    'Хвосты',
                                    'Цепи и карабины',
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                elem: 'col',
                mods: {sm: '9'},
                content: [
                    {
                        tag: 'p',
                        content: 'Ощущение мира решительно подчеркивает дедуктивный метод, хотя в официозе принято обратное. Созерцание непредвзято подрывает естественный бабувизм, отрицая очевидное. Априори, созерцание амбивалентно. Принцип восприятия, по определению, раскладывает на элементы сложный бабувизм, ломая рамки привычных представлений. Адживика осмысленно понимает под собой дуализм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Мир дискредитирует закон исключённого третьего, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.'
                    },
                    {
                        tag: 'h2',
                        content: 'Заголовок h2 — Второстепенный заголовок страницына две строки. Второстепенный заголовок страницына две строки. Второстепенный'
                    },
                    {
                        tag: 'p',
                        content: 'Ощущение мира решительно подчеркивает дедуктивный метод, хотя в официозе принято обратное. Созерцание непредвзято подрывает естественный бабувизм, отрицая очевидное. Априори, созерцание амбивалентно. Принцип восприятия, по определению, раскладывает на элементы сложный бабувизм, ломая рамки привычных представлений. Адживика осмысленно понимает под собой дуализм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Мир дискредитирует закон исключённого третьего, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.'
                    },
                    {
                        tag: 'h3',
                        content: 'Заголовок h3 — Второстепенный заголовок страницына две строки. Второстепенный заголовок страницына две строки. Второстепенный'
                    },
                    {
                        tag: 'p',
                        content: 'Ощущение мира решительно подчеркивает дедуктивный метод, хотя в официозе принято обратное. Созерцание непредвзято подрывает естественный бабувизм, отрицая очевидное. Априори, созерцание амбивалентно. Принцип восприятия, по определению, раскладывает на элементы сложный бабувизм, ломая рамки привычных представлений. Адживика осмысленно понимает под собой дуализм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Мир дискредитирует закон исключённого третьего, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.'
                    },
                    {
                        tag: 'h4',
                        content: 'Заголовок h4 — Второстепенный заголовок страницына две строки. Второстепенный заголовок страницына две строки. Второстепенный'
                    },
                    {
                        tag: 'p',
                        content: 'Ощущение мира решительно подчеркивает дедуктивный метод, хотя в официозе принято обратное. Созерцание непредвзято подрывает естественный бабувизм, отрицая очевидное. Априори, созерцание амбивалентно. Принцип восприятия, по определению, раскладывает на элементы сложный бабувизм, ломая рамки привычных представлений. Адживика осмысленно понимает под собой дуализм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Мир дискредитирует закон исключённого третьего, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.'
                    },
                    {
                        tag: 'h5',
                        content: 'Заголовок h5 — Второстепенный заголовок страницына две строки. Второстепенный заголовок страницына две строки. Второстепенный'
                    },
                    {
                        tag: 'p',
                        content: 'Ощущение мира решительно подчеркивает дедуктивный метод, хотя в официозе принято обратное. Созерцание непредвзято подрывает естественный бабувизм, отрицая очевидное. Априори, созерцание амбивалентно. Принцип восприятия, по определению, раскладывает на элементы сложный бабувизм, ломая рамки привычных представлений. Адживика осмысленно понимает под собой дуализм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Мир дискредитирует закон исключённого третьего, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.'
                    },
                    {
                        tag: 'h6',
                        content: 'Заголовок h6 — Второстепенный заголовок страницына две строки. Второстепенный заголовок страницына две строки. Второстепенный'
                    },
                    {
                        tag: 'p',
                        content: 'Ощущение мира решительно подчеркивает дедуктивный метод, хотя в официозе принято обратное. Созерцание непредвзято подрывает естественный бабувизм, отрицая очевидное. Априори, созерцание амбивалентно. Принцип восприятия, по определению, раскладывает на элементы сложный бабувизм, ломая рамки привычных представлений. Адживика осмысленно понимает под собой дуализм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Мир дискредитирует закон исключённого третьего, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.'
                    },
                    {tag: 'hr'},
                    {
                        block: 'image-block', mix: {block: 'pull-left'}, src: 'http://placehold.it/360x240', content: [
                        '<a href="#">Ощущение мира</a> – решительно подчеркивает дедуктивный метод, хотя в официозе принято обратное.'
                    ]
                    },

                    {
                        tag: 'p',
                        content: 'Ощущение мира решительно подчеркивает дедуктивный метод, хотя в официозе принято обратное. Созерцание непредвзято подрывает естественный бабувизм, отрицая очевидное. Априори, созерцание амбивалентно. Принцип восприятия, по определению, раскладывает на элементы сложный бабувизм, ломая рамки привычных представлений. Адживика осмысленно понимает под собой дуализм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Мир дискредитирует закон исключённого третьего, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.'
                    },
                    {
                        tag: 'p',
                        content: 'Ощущение мира решительно подчеркивает дедуктивный метод, хотя в официозе принято обратное. Созерцание непредвзято подрывает естественный бабувизм, отрицая очевидное. Априори, созерцание амбивалентно. Принцип восприятия, по определению, раскладывает на элементы сложный бабувизм, ломая рамки привычных представлений. Адживика осмысленно понимает под собой дуализм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Мир дискредитирует закон исключённого третьего, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.'
                    },
                    {tag: 'img', attrs: {src: 'http://placehold.it/400x300', width: 'auto', align: 'right'}},
                    {
                        tag: 'p',
                        content: 'Ощущение мира решительно подчеркивает дедуктивный метод, хотя в официозе принято обратное. Созерцание непредвзято подрывает естественный бабувизм, отрицая очевидное. Априори, созерцание амбивалентно. Принцип восприятия, по определению, раскладывает на элементы сложный бабувизм, ломая рамки привычных представлений. Адживика осмысленно понимает под собой дуализм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Мир дискредитирует закон исключённого третьего, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.'
                    },
                    {
                        tag: 'p',
                        content: 'Ощущение мира решительно подчеркивает дедуктивный метод, хотя в официозе принято обратное. Созерцание непредвзято подрывает естественный бабувизм, отрицая очевидное. Априори, созерцание амбивалентно. Принцип восприятия, по определению, раскладывает на элементы сложный бабувизм, ломая рамки привычных представлений. Адживика осмысленно понимает под собой дуализм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Мир дискредитирует закон исключённого третьего, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.'
                    },

                ]
            }
        ]
    },
    {tag: 'hr'},
    {tag: 'h2', content: 'Кнопки'},
    {
        block: 'row',
        mix: [{block: 'pbl'}],
        content: [
            {
                elem: 'col',
                mods: {sm: 7},
                content: [
                    {
                        block: 'row',
                        content: [
                            {
                                elem: 'col',
                                mods: {sm: 3},
                            },
                            {
                                elem: 'col',
                                mods: {sm: 9},
                                content: [
                                    {
                                        block: 'h4',
                                        mix: [{block: 'text-center'}],
                                        content: 'Варианты размеров'
                                    },
                                    {tag: 'hr'},
                                ]
                            },
                        ]
                    },
                    {
                        block: 'row',
                        content: [
                            {
                                elem: 'col',
                                mods: {sm: 3},
                                content: [
                                    {
                                        block: 'h5',
                                        content: 'xl: '
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {sm: 9},
                                content: [
                                    {
                                        block: 'btn',
                                        mods: {size: 'xl'},
                                        content: 'Смотреть каталог'
                                    },
                                ]
                            },
                        ]
                    },
                    {tag: 'br'},
                    {
                        block: 'row',
                        content: [
                            {
                                elem: 'col',
                                mods: {sm: 3},
                                content: [
                                    {
                                        block: 'h5',
                                        content: 'lg: '
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {sm: 9},
                                content: [
                                    {
                                        block: 'btn',
                                        mods: {size: 'lg'},
                                        content: 'Смотреть каталог'
                                    },
                                ]
                            },
                        ]
                    },
                    {tag: 'br'},
                    {
                        block: 'row',
                        content: [
                            {
                                elem: 'col',
                                mods: {sm: 3},
                                content: [
                                    {
                                        block: 'h5',
                                        content: 'normal: '
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {sm: 9},
                                content: [
                                    {
                                        block: 'btn',
                                        content: 'Смотреть каталог'
                                    },
                                ]
                            },
                        ]
                    },
                    {tag: 'br'},
                    {
                        block: 'row',
                        content: [
                            {
                                elem: 'col',
                                mods: {sm: 3},
                                content: [
                                    {
                                        block: 'h5',
                                        content: 'sm: '
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {sm: 9},
                                content: [
                                    {
                                        block: 'btn',
                                        mods: {size: 'sm'},
                                        content: 'Смотреть каталог'
                                    },
                                ]
                            },
                        ]
                    },
                    {tag: 'br'},
                    {
                        block: 'row',
                        content: [
                            {
                                elem: 'col',
                                mods: {sm: 3},
                                content: [
                                    {
                                        block: 'h5',
                                        content: 'xs: '
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {sm: 9},
                                content: [
                                    {
                                        block: 'btn',
                                        mods: {size: 'xs'},
                                        content: 'Смотреть каталог'
                                    },
                                ]
                            },
                        ]
                    },
                ]
            },
            {
                elem: 'col',
                mods: {sm: 5},
                content: [
                    {
                        block: 'h4',
                        mix: [{block: 'text-center'}],
                        content: 'Другие варианты'
                    },
                    {tag: 'hr'},
                    {
                        block: 'btn', mods: {color: 'primary'}, content: [
                        'Смотреть каталог'
                    ]
                    },
                    {tag: 'br'}, {tag: 'br'},
                    {
                        block: 'btn', mods: {color: 'success'}, content: [
                        'Смотреть каталог'
                    ]
                    },
                    {tag: 'br'}, {tag: 'br'},
                    {
                        block: 'btn', mods: {color: 'warning'}, content: [
                        'Смотреть каталог'
                    ]
                    },
                    {tag: 'br'}, {tag: 'br'},
                    {
                        block: 'btn', mods: {color: 'info'}, content: [
                        'Смотреть каталог'
                    ]
                    },
                ]
            }
        ]
    },
    {tag: 'h2', content: 'Списки'},
    {
        block: 'row',
        content: [
            {
                elem: 'col',
                mods: {xs: 12, sm: 4},
                content: [
                    {
                        block: 'h4',
                        content: 'Первый вид'
                    },
                    {
                        block: 'h6',
                        mix: [{block: 'mtn'}],
                        content: 'список без маркера'
                    },
                    {
                        block: 'list-unstyled',
                        mods: {size: 'lg'},
                        tag: 'ul',
                        content: [
                            {tag: 'li', content: 'Calvin Klein'},
                            {tag: 'li', content: 'Tissot'},
                            {tag: 'li', content: 'Certina'},
                            {tag: 'li', content: 'Hamilton'},
                            {tag: 'li', content: 'Rado'},
                            {tag: 'li', content: 'Longines'},
                            {tag: 'li', content: 'Frederique Constant'},
                        ]
                    }
                ]
            },
            {
                elem: 'col',
                mods: {xs: 12, sm: 4},
                content: [
                    {
                        block: 'h4',
                        content: 'Второй вид'
                    },
                    {
                        block: 'h6',
                        mix: [{block: 'mtn'}],
                        content: 'маркерованный'
                    },
                    {
                        tag: 'ul',
                        content: [
                            {tag: 'li', content: 'Calvin Klein'},
                            {tag: 'li', content: [
                                'Tissot',
                                {tag: 'ul',
                                    content: [
                                        {tag: 'li', content: 'Certina'},
                                        {tag: 'li', content: 'Hamilton'},
                                    ]
                                }]
                            },
                            {tag: 'li', content: 'Certina'},
                            {tag: 'li', content: 'Hamilton'},
                            {tag: 'li', content: 'Rado'},
                            {tag: 'li', content: 'Longines'},
                            {tag: 'li', content: 'Frederique Constant'},
                        ]
                    }
                ]
            },
            {
                elem: 'col',
                mods: {xs: 12, sm: 4},
                content: [
                    {
                        block: 'h4',
                        content: 'Третий вид'
                    },
                    {
                        block: 'h6',
                        mix: [{block: 'mtn'}],
                        content: 'нумерованный'
                    },
                    {
                        tag: 'ol',
                        content: [
                            {tag: 'li', content: 'Calvin Klein'},
                            {tag: 'li', content: 'Tissot'},
                            {tag: 'li', content: 'Certina'},
                            {tag: 'li', content: 'Hamilton'},
                            {tag: 'li', content: 'Rado'},
                            {tag: 'li', content: 'Longines'},
                            {tag: 'li', content: 'Frederique Constant'},
                        ]
                    }
                ]
            }
        ]
    },
    {tag: 'h2', content: 'Ссылки'},
    {
        block: 'row',
        content: [
            {
                elem: 'col',
                mods: {sm: '6'},
                content: [
                    {block: 'h4', content: 'Первый вид'},
                    {
                        tag: 'a',
                        attrs: {
                            href: '#'
                        },
                        content: 'castlerock.ru'
                    },
                    {tag: 'br'},
                    {tag: 'br'},
                    {tag: 'br'},
                    {
                        block: 'link',
                        content: 'castlerock.ru'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {sm: '6'},
                content: [
                    {block: 'h4', content: 'Первый вид'},
                    {
                        block: 'demo-for-link',
                        content: [
                            {
                                block: 'link',
                                mods: {white:true},
                                content: 'castlerock.ru'
                            }
                        ]
                    }
                   
                ]
            }
        ]
    },
    {tag: 'h2', content: 'Формы'},
    {
        block: 'row',
        content: [
            {
                elem: 'col',
                mods: {xs: 12, sm: 6},
                content: [
                    {
                        block: 'checkbox',
                        mods: {custom: true},
                        content: 'Акции'
                    },
                    {
                        block: 'checkbox',
                        mods: {custom: true},
                        checked: 'checked',
                        content: 'Акции'
                    },
                    {
                        block: 'checkbox',
                        mods: {custom: true, disabled: true},
                        content: 'Акции'
                    },
                ]
            },
            {
                elem: 'col',
                mods: {xs: 12, sm: 6},
                content: [
                    {
                        block: 'radio',
                        mods: {custom: true},
                        content: 'Акции'
                    },
                    {
                        block: 'radio',
                        mods: {custom: true},
                        checked: 'checked',
                        content: 'Акции'
                    },
                    {
                        block: 'radio',
                        mods: {custom: true, disabled: true},
                        content: 'Акции'
                    },
                ]
            },
        ]
    },
    {tag: 'hr'},
    {
        block: 'form-horizontal',
        mix: [{block: 'mbl'}],
        content: [
            {
                block: 'form-group',
                content: [
                    {
                        block: 'control-label',
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 4, lg: 2}},
                        content: 'Выбирите пользователя'
                    },
                    {
                        block: 'control-input',
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 8, lg: 10}},
                        content: [
                            {
                                block: 'select',
                                mods: {custom: true},
                                content: [
                                    'Профиль 1',
                                    'Профиль 2',
                                    'Профиль 3',
                                    'Профиль 4',
                                    'Профиль 5',
                                    'Профиль 6',
                                ]
                            }
                        ]
                    },
                ]
            },
            {tag: 'h3', content: 'Обратная связь'},
            {
                block: 'form-group',
                content: [
                    {
                        elem: 'col',
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 6}},
                        content: [
                            {
                                block: 'form-group',
                                content: [
                                    {
                                        block: 'control-label',
                                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 4}},
                                        content: 'Ваше имя*'
                                    },
                                    {
                                        block: 'control-input',
                                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 8}},
                                        content: {
                                            block: 'input',
                                            mods: {control: true},
                                        }
                                    }
                                ]
                            },
                            {
                                block: 'form-group',
                                content: [
                                    {
                                        block: 'control-label',
                                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 4}},
                                        content: 'Контактный телефон'
                                    },
                                    {
                                        block: 'control-input',
                                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 8}},
                                        content: {
                                            block: 'input',
                                            mods: {control: true},
                                        }
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        elem: 'col',
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 6}},
                        content: [
                            {
                                block: 'form-group',
                                content: [
                                    {
                                        block: 'control-label',
                                        mix: {
                                            block: 'row',
                                            elem: 'col',
                                            mods: {xs: 12, sm: 3, 'sm-offset': 1, md: 2, 'md-offset': 2}
                                        },
                                        content: 'E-mail*'
                                    },
                                    {
                                        block: 'control-input',
                                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 8}},
                                        content: {
                                            block: 'input',
                                            mods: {control: true},
                                        }
                                    }
                                ]
                            },
                            {
                                block: 'form-group',
                                content: [
                                    {
                                        block: 'control-label',
                                        mix: {
                                            block: 'row',
                                            elem: 'col',
                                            mods: {xs: 12, sm: 3, 'sm-offset': 1, md: 2, 'md-offset': 2}
                                        },
                                        content: 'Город'
                                    },
                                    {
                                        block: 'control-input',
                                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 8}},
                                        content: {
                                            block: 'input',
                                            mods: {control: true},
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                block: 'form-group',
                content: [
                    {
                        block: 'control-label',
                        mix: [{block: 'row', elem: 'col', mods: {xs: 12}}, {block: 'visible-xs'}],
                        content: 'Сообщение:'
                    },
                    {
                        block: 'control-input',
                        mix: {block: 'row', elem: 'col', mods: {xs: 12}},
                        content: {
                            block: 'textarea',
                        }
                    }
                ]
            },
            {
                block: 'form-group',
                mix: [{block: 'ptm'}],
                content: [
                    {
                        block: 'control-label',
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 2}},
                        content: 'Символы с картинки*'
                    },
                    {
                        block: 'control-input',
                        mix: {block: 'row', elem: 'col', mods: {xs: 8, 'xs-push': 4, sm: 4, 'sm-push': 0, md: 4, lg: 6}},
                        content: {
                            block: 'input',
                            mods: {control: true},
                        }
                    },
                    {
                        block: 'control-input',
                        mix: {block: 'row', elem: 'col', mods: {xs: 4, 'xs-pull': 8, sm: 2, 'sm-pull': 0, md: 2}},
                        content: {
                            block: 'img',
                            url: 'captcha.png'
                        }
                    },
                    {
                        block: 'control-input',
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 4, md: 3, lg: 2}},
                        content: {
                            block: 'btn',
                            mods: {color: 'danger', block: true},
                            content: 'Отправить'
                        }
                    }
                ]
            }
        ]
    }
];