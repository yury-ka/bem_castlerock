module.exports = [
    {
        block: 'block',
        mods: {theme: 'footer'},
        attrs: {
            'data-theme': 'dark'
        },
        content: [
            {
                block: 'container',
                content: [
                    {
                        block: 'row',
                        content: [
                            {
                                elem: 'col',
                                mods: {md: 4, sm: 6},
                                content: [
                                    {
                                        block: 'logo',
                                        mods: {'auto-title': false},
                                        title: 'Старейший культовый рок-магазин'
                                    },
                                    {
                                        block: 'subscription',
                                        placeholder: 'Подпишись на новости',
                                        btn: {
                                            block: 'img',
                                            url: 'subscription.png'
                                        }
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {md: 2, sm: 3},
                                content: [
                                    {
                                        block: 'footer-title',
                                        content: 'О компании'
                                    },
                                    {
                                        block: 'ul',
                                        mix: [{block: 'footer-menu'}],
                                        content: [
                                            {
                                                elem: 'link',
                                                content: 'О компании'
                                            },
                                            {
                                                elem: 'link',
                                                content: 'Рок-Блог'
                                            },
                                            {
                                                elem: 'link',
                                                content: 'Контакты'
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {md: 2, sm: 3},
                                content: [
                                    {
                                        block: 'footer-title',
                                        content: 'Условия покупки'
                                    },
                                    {
                                        block: 'ul',
                                        mix: [{block: 'footer-menu'}],
                                        content: [
                                            {
                                                elem: 'link',
                                                content: 'Оплата'
                                            },
                                            {
                                                elem: 'link',
                                                content: 'Доставка'
                                            },
                                            {
                                                elem: 'link',
                                                content: 'Гарантия'
                                            },
                                            {
                                                elem: 'link',
                                                content: 'FAQ'
                                            },
                                            {
                                                elem: 'link',
                                                content: 'Где мой заказ?'
                                            },
                                            {
                                                elem: 'link',
                                                content: 'Юридическая информация'
                                            },
                                        ]
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {md: '4'},
                                content: [
                                    {
                                        block: 'row',
                                        content: [
                                            {
                                                elem: 'col',
                                                mods: {lg: 6, md: 12, sm: 6},
                                                content: [
                                                    {
                                                        block: 'vk-group',
                                                        attrs: {
                                                            id: 'widgets-group'
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                elem: 'col',
                                                mods: {lg: 6, md: 12, sm: 6},
                                                content: [
                                                    {
                                                        block: 'vk-group',
                                                        attrs: {
                                                            id: 'widgets-group2'
                                                        }
                                                    }
                                                ]
                                            },
                                        ]
                                    }
                                ]
                            }


                        ]
                    },
                    {
                        block: 'row',
                        content: [
                            {
                                elem: 'col',
                                mods: {sm: 4},
                                content: [
                                    {
                                        block: 'footer-bottom-item',
                                        content: '© ЗАО "Рашн Веб Трейд" 2011-2016 <br/>© ООО "CastleRock" 1992-2016'
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {sm: 5, md: 4},
                                content: [
                                    {
                                        block: 'footer-bottom-item',
                                        mods: {align: 'center'},
                                        content: [
                                            '<span class="mrx">Мы в соцсетях </span>',
                                            {
                                                block: 'link',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        mix: [{block: 'mrx'}],
                                                        url:'soc/f-mini.png'
                                                    },
                                                ]
                                            },
                                            {
                                                block: 'link',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        mix: [{block: 'mrx'}],
                                                        url:'soc/g-mini.png'
                                                    },
                                                ]
                                            },
                                            {
                                                block: 'link',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        mix: [{block: 'mrx'}],
                                                        url:'soc/vk-mini.png'
                                                    },
                                                ]
                                            },
                                            {
                                                block: 'link',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        mix: [{block: 'mrx'}],
                                                        url:'soc/ok-mini.png'
                                                    },
                                                ]
                                            },
                                        ]
                                    }
                                ]
                            },
                            {
                                elem: 'col',
                                mods: {sm: 3, md: 4},
                                content: [
                                    {
                                        block: 'footer-bottom-item',
                                        mods: {align: 'right'},
                                        content: {
                                            block: 'footer-address',
                                            content: [
                                                {
                                                    elem: 'item',
                                                    content: '<span>СПб, Лиговский, 47: </span>+7 (812) 322-65-68'
                                                },
                                                {
                                                    elem: 'item',
                                                    content: '<span>Интернет магазин: </span>+7 (812) 938-78-75'
                                                }
                                            ]
                                        }
                                    }
                                ]
                            },
                        ]
                    }
                ]
            },
            {
                block: 'to-up'
            }
        ]
    },
];
