module.exports = function () {
    var content = [];

    var bemjson = {
        block: 'blog-preview-line',
        content: [
            {
                elem: 'img-wrap',
                content: [
                    {
                        block: 'img',
                        mix: [{block: 'blog-preview-line', elem: 'img'}],
                        mods: {lazyload: true, responsive: true},
                        url: 'blog/img-1.jpg'
                    }
                ]
            },
            {
                elem: 'content',
                content: [
                    {
                        elem: 'title',
                        mods: {date: true},
                        content: 'Дэвид Дрейман против Бейонсе. Кто кого?'
                    },
                    {
                        elem: 'date',
                        content: '16.12.2016'
                    },
                    {
                        block: 'tag-list',
                        content: [
                            {
                                elem: 'label',
                                content: 'Теги:'
                            },
                            {
                                elem: 'item',
                                content: 'Disturbed'
                            },
                            {
                                elem: 'item',
                                content: 'Twenty One Pilots'
                            },
                            {
                                elem: 'item',
                                content: 'Disturbed'
                            },
                            {
                                elem: 'item',
                                content: 'Twenty One Pilots'
                            },
                            {
                                elem: 'item',
                                content: 'Disturbed'
                            },
                        ]
                    },
                    {
                        elem: 'text',
                        content: [
                            'Серией концертов по России прокатилась шведская тяжёлая бронированная машина SABATON. Мы, конечно, не остались в стороне и посетили концерт 9 декабря в Питере в клубе A2 Green Concert. Сразу нужно отметить, что это мероприятие вызвало интерес у нашей металлической общественности, так как достаточно крупная концертная площадка была заполнена.'
                        ]
                    },
                    {
                        elem: 'readmore',
                        content: 'Читать далее'
                    }
                ]
            },

        ]
    }

    for (var i = 0; i < 6; i++) {
        content.push(bemjson);
    }
    return [
        content
    ];
}