module.exports = [
    {
        block: 'modal',
        mix: [{block: 'fade'}],
        attrs: {
            id: 'quick-view-modal'
        },
        mods: {'quick-view': true},
        content: [
            {
                elem: 'dialog',
                mods: {size: 'lg'},
                content: [
                    {
                        elem: 'content',
                        content: [
                            {
                                elem: 'close'
                            },
                            {
                                block: 'quick-view',
                                content: [
                                    {
                                        elem: 'left'
                                    },
                                    {
                                        elem: 'right'
                                    },
                                    {
                                        elem: 'col',
                                        mods: {for: 'slider'},
                                        content: [
                                            {
                                                block: 'slider-detail',
                                                content: [
                                                    {
                                                        elem: 'big',
                                                        mods: {'quick-view':true},
                                                        content: [
                                                            {
                                                                elem: 'item',
                                                                content: [
                                                                    {
                                                                        block: 'img',
                                                                        mix: [{block: 'slider-detail', elem: 'img'}],
                                                                        mods: {responsive: true},
                                                                        url: 'product/img-big-1.jpg'
                                                                    }
                                                                ]
                                                            },
                                                            {
                                                                elem: 'item',
                                                                content: [
                                                                    {
                                                                        block: 'img',
                                                                        mix: [{block: 'slider-detail', elem: 'img'}],
                                                                        mods: { responsive: true},
                                                                        url: 'product/img-big-2.jpg'
                                                                    }
                                                                ]
                                                            },
                                                            {
                                                                elem: 'item',
                                                                content: [
                                                                    {
                                                                        block: 'img',
                                                                        mix: [{block: 'slider-detail', elem: 'img'}],
                                                                        mods: {responsive: true},
                                                                        url: 'product/img-big-3.jpg'
                                                                    }
                                                                ]
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        elem: 'nav',
                                                        mods: {'quick-view':true},
                                                        content: [
                                                            {
                                                                elem: 'nav-item',
                                                                content: [
                                                                    {
                                                                        block: 'img',
                                                                        mods: {responsive: true},
                                                                        url: 'product/img-min-1.jpg'
                                                                    }
                                                                ]
                                                            },
                                                            {
                                                                elem: 'nav-item',
                                                                content: [
                                                                    {
                                                                        block: 'img',
                                                                        mods: {responsive: true},
                                                                        url: 'product/img-min-2.jpg'
                                                                    }
                                                                ]
                                                            },
                                                            {
                                                                elem: 'nav-item',
                                                                content: [
                                                                    {
                                                                        block: 'img',
                                                                        mods: {responsive: true},
                                                                        url: 'product/img-min-3.jpg'
                                                                    }
                                                                ]
                                                            },
                                                        ]
                                                    },

                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        elem: 'col',
                                        mods: {for: 'control'},
                                        content: [
                                            {
                                                block: 'product-control',
                                                mods: {'quick-view': true},
                                                attrs: {
                                                    id: 'product-control_detail'
                                                },
                                                content: [
                                                    {
                                                        elem: 'title',
                                                        content: 'Туника Pink Floyd'
                                                    },
                                                    {
                                                        elem: 'last-in-stock',
                                                        content: 'Последний товар в наличии!'
                                                    },
                                                    {
                                                        elem: 'size',
                                                        content: [
                                                            {
                                                                elem: 'size-item',
                                                                mods: {stock: true},
                                                                content: 'S'
                                                            },
                                                            {
                                                                elem: 'size-item',
                                                                mods: {stock: true},
                                                                content: 'M'
                                                            },
                                                            {
                                                                elem: 'size-item',
                                                                mods: {stock: true},
                                                                content: 'L'
                                                            },
                                                            {
                                                                elem: 'size-item',
                                                                content: 'XL'
                                                            },
                                                            {
                                                                elem: 'size-item',
                                                                mods: {stock: true},
                                                                content: 'XXL'
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        elem: 'old-price',
                                                        content: '<s>3 150 ₽</s>'
                                                    },
                                                    {
                                                        elem: 'price',
                                                        content: [
                                                            '<b>2 990</b> ₽',
                                                        ]
                                                    },
                                                    {
                                                        elem: 'row-to-cart',
                                                        content: [
                                                            {
                                                                block: 'btn',
                                                                mix: [{block: 'product-control', elem: 'to-cart'}],
                                                                content: 'Добавить в корзину'
                                                            },

                                                        ]
                                                    },
                                                    {
                                                        elem: 'favorites',
                                                        content: 'В избранное'
                                                    },
                                                    {
                                                        block: 'quick-view-info',
                                                        content: [
                                                            {
                                                                elem: 'row',
                                                                content: [
                                                                    {
                                                                        elem: 'col',
                                                                        mods: {for: 'label'},
                                                                        content: 'Артикул:'
                                                                    },
                                                                    {
                                                                        elem: 'col',
                                                                        mods: {for: 'info'},
                                                                        content: '01160077'
                                                                    }
                                                                ]
                                                            },
                                                            {
                                                                elem: 'row',
                                                                content: [
                                                                    {
                                                                        elem: 'col',
                                                                        mods: {for: 'label'},
                                                                        content: 'Производитель:'
                                                                    },
                                                                    {
                                                                        elem: 'col',
                                                                        mods: {for: 'info'},
                                                                        content: 'RockWay'
                                                                    }
                                                                ]
                                                            },
                                                            {
                                                                elem: 'row',
                                                                content: [
                                                                    {
                                                                        elem: 'col',
                                                                        mods: {for: 'label'},
                                                                        content: 'Цвет:'
                                                                    },
                                                                    {
                                                                        elem: 'col',
                                                                        mods: {for: 'info'},
                                                                        content: 'Черный'
                                                                    }
                                                                ]
                                                            },
                                                        ]
                                                    }
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        block: 'btn',
                                        mix: [{block: 'quick-view', elem: 'btn'}],
                                        mods: {color: 'info', block: true},
                                        content: 'Узнать больше'
                                    }
                                ]
                            },
                            
                        ]
                    }
                ]
            }
        ]
    }
]