module.exports = [
    {
        block: 'preview-blog',
        content: [
            {
                elem: 'img',
                content: [
                    {
                        block: 'img',
                        mods: {lazyload: true, responsive: true},
                        url: 'blog/img-1_small.jpg'
                    }
                ]
            },
            {
                elem: 'title',
                content: 'Мировой тур Ганзов в самом разгаре'
            },
            {
                elem: 'text',
                content: 'GUNS N ROSES продолжают свой фееричный реюньон-тур "Not In This Lifetime". Закончив выступления в США, после краткосрочного отдыха группа перебралась в Южную Америку.'
            },
            {
                elem: 'readmore',
                content: 'Читать далее'
            }
        ]
    }
]