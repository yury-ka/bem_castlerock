module.exports = function (nouser) {
var bemjsonUser;
    if (!nouser) {
        bemjsonUser =   {
            block: 'dropdown',
            mods: {user: true},
            title: '<span>Антон Колодницкий</span>',
            content: [
                {
                    elem: 'item',
                    content: 'Заказы'
                },
                {
                    elem: 'item',
                    content: 'Бонусы'
                },
                {
                    elem: 'hr'
                },
                {
                    elem: 'item',
                    content: 'Профиль'
                },
                {
                    elem: 'item',
                    content: 'Выйти'
                }
            ]
        }
    } else {
        bemjsonUser =   {
            block: 'dropdown',
            mods: {'user': true},
            title: '<span>Войти</span>',
            content: [
                {
                    block: 'authorization',
                    content: [
                        {
                            elem: 'title',
                            content: 'Вход в личный кабинет'
                        },
                        {
                            block: 'input',
                            mix: [{block: 'authorization', elem: 'input'}],
                            mods: {control: true},
                            placeholder: 'Логин',
                            type: 'text'
                        },
                        {
                            block: 'input',
                            mix: [{block: 'authorization', elem: 'input'}],
                            mods: {control: true},
                            placeholder: 'Пароль',
                            type: 'password'
                        },
                        {
                            elem: 'wrap-control',
                            content: [
                                {
                                    block: 'btn',
                                    mix: [{block: 'authorization', elem: 'btn'}, {block: 'pull-left'}],
                                    mods: {color: 'primary', size: 'sm'},
                                    content: 'Войти'
                                },
                                {
                                    block: 'checkbox',
                                    mix: [{block: 'authorization', elem: 'checkbox'}, {block: 'pull-right'}],
                                    mods: {custom: true},
                                    content: 'Запомнить меня'
                                },
                            ]
                        },
                        {
                            block: 'clearfix',
                            content: [
                                {
                                    block: 'link',
                                    mix: [{block: 'authorization', elem: 'link'}, {block: 'pull-left'}],
                                    content: 'Регистрация'
                                },
                                {
                                    block: 'link',
                                    mix: [{block: 'authorization', elem: 'link'}, {block: 'pull-right'}],
                                    content: 'Забыли пароль?'
                                },
                            ]
                        }


                    ]
                }
            ]
        }
    }
    return [
        {
            block: 'block',
            mods: {theme: 'dark'},
            attrs: {
                'data-theme': 'dark'
            },
            content: [
                {
                    block: 'sidebar-flying',
                    content: [
                        {
                            elem: 'close',
                            content: '<i class="fa fa-times" aria-hidden="true"></i>'
                        },
                        {
                            block: 'phone',
                            content: [
                                {
                                    elem: 'numb',
                                    content: '+7 (812) 938-78-75'
                                },
                                {
                                    elem: 'icon',
                                    content: '<span class="fa-stack"><i class="fa fa-circle-thin fa-stack-2x fa-inverse"></i><i class="fa fa-exclamation fa-stack-1x fa-inverse"></i></span>'
                                },
                                {
                                    elem: 'caption',
                                    content: 'Интернет-магазин'
                                },
                            ]
                        },
                        {
                            block: 'phone',
                            content: [
                                {
                                    elem: 'numb',
                                    content: '+7 (812) 322-65-68'
                                },
                                {
                                    elem: 'icon',
                                    content: '<span class="fa-stack"><i class="fa fa-circle-thin fa-stack-2x fa-inverse"></i><i class="fa fa-exclamation fa-stack-1x fa-inverse"></i></span>'
                                },
                                {
                                    elem: 'caption',
                                    content: '<a href="#">Спб. Лиговский 47</a>'
                                }
                            ]
                        },
                        {
                            block: 'feedback',
                            content: 'Мы вам <br/>перезвоним'
                        },
                        {
                            block: 'ul',
                            mix: [{block: 'sidebar-menu'}, {block: 'mtl'}],
                            content: [
                                {
                                    elem: 'link',
                                    content: 'Оплата'
                                },
                                {
                                    elem: 'link',
                                    content: 'Доставка'
                                },
                                {
                                    elem: 'link',
                                    content: 'Гарантия'
                                },
                                {
                                    elem: 'link',
                                    content: 'FAQ'
                                },
                                {
                                    elem: 'link',
                                    content: 'Где мой заказ?'
                                },
                            ]
                        }
                    ]
                },
                {
                    block: 'header-top',
                    content: [
                        {
                            block: 'container',
                            content: [
                                {
                                    block: 'header-line',
                                    content: [
                                        {
                                            elem: 'item',
                                            mods: {is: 'btn-side'},
                                            content: [
                                                {
                                                    block: 'btn-side',
                                                    content: 'Контакты'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'item',
                                            mods: {is: 'city'},
                                            content: [
                                                {
                                                    block: 'city',
                                                    content: [
                                                        {
                                                            elem: 'label',
                                                            content: 'Ваш город: '
                                                        },
                                                        {
                                                            elem: 'your',
                                                            content: 'Петропаловск Камчатск'
                                                        },
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'item',
                                            mods: {is: 'phone'},
                                            content: [
                                                {
                                                    block: 'phone',
                                                    content: [
                                                        {
                                                            elem: 'numb',
                                                            content: '+7 (812) 938-78-75'
                                                        },
                                                        {
                                                            elem: 'icon',
                                                            content: '<span class="fa-stack"><i class="fa fa-circle-thin fa-stack-2x fa-inverse"></i><i class="fa fa-exclamation fa-stack-1x fa-inverse"></i></span>'
                                                        },
                                                        {
                                                            elem: 'caption',
                                                            content: 'Интернет-магазин'
                                                        }
                                                    ]
                                                },
                                            ]
                                        },
                                        {
                                            elem: 'item',
                                            mods: {is: 'phone'},
                                            content: [
                                                {
                                                    block: 'phone',
                                                    content: [
                                                        {
                                                            elem: 'numb',
                                                            content: '+7 (812) 322-65-68'
                                                        },
                                                        {
                                                            elem: 'icon',
                                                            content: '<span class="fa-stack"><i class="fa fa-circle-thin fa-stack-2x fa-inverse"></i><i class="fa fa-exclamation fa-stack-1x fa-inverse"></i></span>'
                                                        },
                                                        {
                                                            elem: 'caption',
                                                            content: '<a href="#">Спб. Лиговский 47</a>'
                                                        }
                                                    ]
                                                },
                                            ]
                                        },
                                        {
                                            elem: 'item',
                                            mods: {is: 'feedback'},
                                            content: [
                                                {
                                                    block: 'feedback',
                                                    content: 'Мы вам <br/>перезвоним'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'item',
                                            mods: {is: 'user'},
                                            content: [
                                                bemjsonUser
                                            ]
                                        }
                                    ]
                                }

                            ]
                        }
                    ]
                },
                {
                    block: 'container',
                    mods: {'header-control':true},
                    content: [
                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '4'},
                                    content: [
                                        {
                                            block: 'logo',
                                            mods: {'auto-title': true}
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '8'},
                                    content: [
                                        {
                                            block: 'advantages',
                                            mix: [{block: 'hidden-xs'}],
                                            content: [
                                                {
                                                    elem: 'item',
                                                    mods: {icon: 'plane'},
                                                    content: 'Доставка <br/>по всей России'
                                                },
                                                {
                                                    elem: 'item',
                                                    mods: {icon: 'wallet'},
                                                    content: 'Безопасная <br/>и удобная оплата'
                                                },
                                                {
                                                    elem: 'item',
                                                    mods: {icon: 'clock'},
                                                    content: 'Отправка <br/>в день заказа'
                                                },
                                            ]
                                        },
                                        {
                                            block: 'row',
                                            content: [
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '8', md: '7', sm: '6', xs: '12', 'sm-offset': '2', 'md-offset': '0'},
                                                    content: [
                                                        {
                                                            block: 'search',
                                                            mods: {header:true},
                                                            icon: 'fa',
                                                            placeholder: 'Поиск среди 100 000 товаров в наличии'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {sm: '1', xs: '2', 'xs-offset': '2', 'sm-offset': '0'},
                                                    content: [
                                                        {
                                                            block: 'favorites',
                                                            mods: {header: true},
                                                            mix: [{block: 'hidden-xs'}],
                                                            content: '5'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '4', sm: '3', xs: '8'},
                                                    content: [
                                                        {
                                                            block: 'cart',
                                                            mix: [{block: 'dropdown'}],
                                                            content: [
                                                                {
                                                                    elem: 'btn',
                                                                    tag: 'button',
                                                                    attrs: {
                                                                        type: 'botton',
                                                                        'data-toggle':'dropdown'
                                                                    },
                                                                    content: '3 на 110 500 ₽'
                                                                },
                                                                {
                                                                    elem: 'block',
                                                                    mix: [{block: 'dropdown-menu'}],
                                                                    content: [
                                                                        {
                                                                            elem: 'list',
                                                                            content: [
                                                                                {
                                                                                    elem: 'item',
                                                                                    content: [
                                                                                        {
                                                                                            block: 'img',
                                                                                            mix: [{block: 'cart', elem: 'img'}],
                                                                                            mods: {responsive: true},
                                                                                            url: 'cart/img-1.jpg'
                                                                                        },
                                                                                        {
                                                                                            elem: 'info',
                                                                                            content: [
                                                                                                {
                                                                                                    elem: 'title',
                                                                                                    content: 'Туника Pink Floyd'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'price',
                                                                                                    content: '8 800 ₽'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'size',
                                                                                                    content: 'S'
                                                                                                }
                                                                                            ]
                                                                                        }
                                                                                    ]
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    content: [
                                                                                        {
                                                                                            block: 'img',
                                                                                            mix: [{block: 'cart', elem: 'img'}],
                                                                                            mods: {responsive: true},
                                                                                            url: 'cart/img-1.jpg'
                                                                                        },
                                                                                        {
                                                                                            elem: 'info',
                                                                                            content: [
                                                                                                {
                                                                                                    elem: 'title',
                                                                                                    content: 'Туника Pink Floyd'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'price',
                                                                                                    content: '2 800 ₽'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'size',
                                                                                                    content: 'S'
                                                                                                }
                                                                                            ]
                                                                                        }
                                                                                    ]
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    content: [
                                                                                        {
                                                                                            block: 'img',
                                                                                            mix: [{block: 'cart', elem: 'img'}],
                                                                                            mods: {responsive: true},
                                                                                            url: 'cart/img-1.jpg'
                                                                                        },
                                                                                        {
                                                                                            elem: 'info',
                                                                                            content: [
                                                                                                {
                                                                                                    elem: 'title',
                                                                                                    content: 'Туника Pink Floyd'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'price',
                                                                                                    content: '9 800 ₽'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'size',
                                                                                                    content: 'S'
                                                                                                }
                                                                                            ]
                                                                                        }
                                                                                    ]
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    content: [
                                                                                        {
                                                                                            block: 'img',
                                                                                            mix: [{block: 'cart', elem: 'img'}],
                                                                                            mods: {responsive: true},
                                                                                            url: 'cart/img-1.jpg'
                                                                                        },
                                                                                        {
                                                                                            elem: 'info',
                                                                                            content: [
                                                                                                {
                                                                                                    elem: 'title',
                                                                                                    content: 'Туника Pink Floyd'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'price',
                                                                                                    content: '7 800 ₽'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'size',
                                                                                                    content: 'S'
                                                                                                }
                                                                                            ]
                                                                                        }
                                                                                    ]
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    content: [
                                                                                        {
                                                                                            block: 'img',
                                                                                            mix: [{block: 'cart', elem: 'img'}],
                                                                                            mods: {responsive: true},
                                                                                            url: 'cart/img-1.jpg'
                                                                                        },
                                                                                        {
                                                                                            elem: 'info',
                                                                                            content: [
                                                                                                {
                                                                                                    elem: 'title',
                                                                                                    content: 'Туника Pink Floyd'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'price',
                                                                                                    content: '4 800 ₽'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'size',
                                                                                                    content: 'S'
                                                                                                }
                                                                                            ]
                                                                                        }
                                                                                    ]
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    content: [
                                                                                        {
                                                                                            block: 'img',
                                                                                            mix: [{block: 'cart', elem: 'img'}],
                                                                                            mods: {responsive: true},
                                                                                            url: 'cart/img-1.jpg'
                                                                                        },
                                                                                        {
                                                                                            elem: 'info',
                                                                                            content: [
                                                                                                {
                                                                                                    elem: 'title',
                                                                                                    content: 'Туника Pink Floyd'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'price',
                                                                                                    content: '3 500 ₽'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'size',
                                                                                                    content: 'S'
                                                                                                }
                                                                                            ]
                                                                                        }
                                                                                    ]
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    content: [
                                                                                        {
                                                                                            block: 'img',
                                                                                            mix: [{block: 'cart', elem: 'img'}],
                                                                                            mods: {responsive: true},
                                                                                            url: 'cart/img-1.jpg'
                                                                                        },
                                                                                        {
                                                                                            elem: 'info',
                                                                                            content: [
                                                                                                {
                                                                                                    elem: 'title',
                                                                                                    content: 'Туника Pink Floyd'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'price',
                                                                                                    content: '34 500 ₽'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'size',
                                                                                                    content: 'S'
                                                                                                }
                                                                                            ]
                                                                                        }
                                                                                    ]
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    content: [
                                                                                        {
                                                                                            block: 'img',
                                                                                            mix: [{block: 'cart', elem: 'img'}],
                                                                                            mods: {responsive: true},
                                                                                            url: 'cart/img-1.jpg'
                                                                                        },
                                                                                        {
                                                                                            elem: 'info',
                                                                                            content: [
                                                                                                {
                                                                                                    elem: 'title',
                                                                                                    content: 'Туника Pink Floyd'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'price',
                                                                                                    content: '7 500 ₽'
                                                                                                },
                                                                                                {
                                                                                                    elem: 'size',
                                                                                                    content: 'S'
                                                                                                }
                                                                                            ]
                                                                                        }
                                                                                    ]
                                                                                },
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'to-cart',
                                                                            mix: [{block: 'btn'}],
                                                                            content: 'Перейти в корзину'
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            block: 'favorites',
                                                            mods: {header: true, mob:true},
                                                            mix: [{block: 'visible-xs-inline-block'}],
                                                            content: '5'
                                                        },
                                                        {
                                                            block: 'btn-search',
                                                            mix: [{block: 'visible-xs-inline-block'}],
                                                            content: '<i class="fa fa-search" aria-hidden="true"></i>'
                                                        },
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                            ]
                        },
                        {
                            block: 'menu',
                            content: [
                                {
                                    elem: 'btn-menu',
                                    content: [
                                        {
                                            block: 'icon-bar'
                                        },
                                        'меню'
                                    ]
                                },
                                {
                                    elem: 'list',
                                    mix: [{block: 'collapse '}],
                                    attrs: {
                                        id: 'menu'
                                    },
                                    content: [
                                        {
                                            elem: 'list-item',
                                            content: [
                                                {
                                                    block: 'catalog',
                                                    mix: [{block: 'dropdown'}],
                                                    content: [
                                                        {
                                                            elem: 'btn',
                                                            attrs: {
                                                                'data-toggle': 'dropdown'
                                                            },
                                                            content: [
                                                                {
                                                                    block: 'icon-bar'
                                                                },
                                                                'КАТАЛОГ'
                                                            ]
                                                        },
                                                        {
                                                            elem: 'block',
                                                            mix: [{block: 'dropdown-menu'}],
                                                            content: [
                                                                {
                                                                    elem: 'list',
                                                                    content: [
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Обувь'
                                                                                },
                                                                                {
                                                                                    elem: 'submenu',
                                                                                    content: [
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'NEW ROCK'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Ботинки'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Казаки'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Полуботинки'
                                                                                        }
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Одежда'
                                                                                },
                                                                                {
                                                                                    elem: 'submenu',
                                                                                    content: [
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Брюки'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            mods: {primary: true},
                                                                                            content: 'Жилеты'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Рубашки, блузы'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Пиджаки'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Куртки'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            mods: {primary: true},
                                                                                            content: 'Майки'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Носки, чулки'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Перчатки'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Платья, туники'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            mods: {primary: true},
                                                                                            content: 'Плащи'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Толстовки'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Женские футболки, Топики'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            mods: {primary: true},
                                                                                            content: 'Шорты'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Юбки'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            mods: {primary: true},
                                                                                            content: 'Футболки с длинным рукавом'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Футболки с коротким рукавом'
                                                                                        },
                                                                                        {
                                                                                            elem: 'subitem',
                                                                                            content: 'Шарфы'
                                                                                        }
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Аксессуары'
                                                                                },
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Головные уборы'
                                                                                },
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Музыкальные инструменты'
                                                                                },
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Детский мир'
                                                                                },
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Печатная продукция'
                                                                                },
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Севениры'
                                                                                },
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Сумки'
                                                                                },
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Украшения'
                                                                                },
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Товары байкерам'
                                                                                },
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'item',
                                                                            content: [
                                                                                {
                                                                                    elem: 'link',
                                                                                    content: 'Интерьер'
                                                                                },
                                                                            ]
                                                                        },
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },

                                                {
                                                    elem: 'point',
                                                },
                                                {
                                                    elem: 'item',
                                                    content: 'Новинки'
                                                },
                                                {
                                                    elem: 'point',
                                                },
                                                {
                                                    elem: 'item',
                                                    content: 'Эксклюзив'
                                                },
                                                {
                                                    elem: 'point',
                                                },
                                                {
                                                    elem: 'item',
                                                    content: 'Обувь под заказ'
                                                },
                                                // {
                                                //     elem: 'point',
                                                // },
                                                // {
                                                //     elem: 'item',
                                                //     content: 'Подарочные сертификаты'
                                                // },
                                                // {
                                                //     elem: 'point',
                                                // },
                                                // {
                                                //     elem: 'item',
                                                //     content: 'Подобрать по группе и тематике'
                                                // },
                                                {
                                                    elem: 'point',
                                                },
                                                {
                                                    elem: 'item',
                                                    mods: {color: 'one'},
                                                    content: 'Акции'
                                                },
                                                {
                                                    elem: 'point',
                                                },
                                                {
                                                    elem: 'item',
                                                    mods: {color: 'two'},
                                                    content: 'Рок-Блог'
                                                }
                                            ]
                                        },
                                        ,
                                    ]
                                },
                            ]
                        }
                    ]
                }
            ]
        },

    ];
}