module.exports = [
    {
        block: 'row',
        content: [
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-1.jpg',
                        title: 'Галстуки, боло'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-2.jpg',
                        title: 'Нашивки'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-3.jpg',
                        title: 'Тату-рукава'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-4.jpg',
                        title: 'Цепи и карабины'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-5.jpg',
                        title: 'Маски'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-6.jpg',
                        title: 'Зеркала'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-7.jpg',
                        title: 'Тату-рукава'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-8.jpg',
                        title: 'Нашивки'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-9.jpg',
                        title: 'Галстуки, боло'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-10.jpg',
                        title: 'Нашивки'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-11.jpg',
                        title: 'Зеркала'
                    }
                ]
            },
            {
                elem: 'col',
                mods: {lg: '2', md: '3', sm: '4', xs: '6'},
                content: [
                    {
                        block: 'category-item',
                        img: 'category-item/img-12.jpg',
                        title: 'Цепи и карабины'
                    }
                ]
            },
        ]
    }
]