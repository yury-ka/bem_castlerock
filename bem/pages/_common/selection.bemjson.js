module.exports = [
    {
        block: 'block',
        mods: {theme: 'selection'},
        attrs: {
            'data-theme': 'dark'
        },
        content: [
            {
                block: 'container',
                content: [
                    {
                        block: 'breadcrumb'
                    },
                    {
                        block: 'title',
                        mods: {first:true, selection: true},
                        mix: [{block: 'h1'}],
                        tag: 'h1',
                        content: 'Подборка товаров'
                    },
                    {
                        block: 'nav-tabs',
                        mods: {selection:true},
                        content: [
                            {
                                elem: 'item',
                                mods: {active: true},
                                for: 'tab-1',
                                content: 'Зарубежные группы'
                            },
                            {
                                elem: 'item',
                                for: 'tab-2',
                                content: 'Российские группы'
                            },
                            {
                                elem: 'item',
                                for: 'tab-3',
                                content: 'Тематика'
                            }
                        ]
                    },
                ]
            },
            {
                block: 'tab-content',
                mods: {selection: true},
                content: [
                    {
                        elem: 'item',
                        mods: {active: true},
                        id: 'tab-1',
                        content: [
                            {
                                block: 'container',
                                content: [
                                    require('../_common/selection-block.bemjson.js'),
                                ]
                            }
                        ]
                    },
                    {
                        elem: 'item',
                        id: 'tab-2',
                        content: [
                            {
                                block: 'container',
                                content: [
                                    require('../_common/selection-block.bemjson.js'),
                                ]
                            }
                        ]
                    },
                    {
                        elem: 'item',
                        id: 'tab-3',
                        content: [
                            {
                                block: 'container',
                                content: [
                                    require('../_common/selection-block.bemjson.js'),
                                ]
                            }
                        ]
                    },

                ]
            },
        ]
    }
]