module.exports = {
    block: 'page',
    title: 'Категории',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../_common/header.bemjson.js')(),

        
        require('../_common/selection.bemjson.js'),
        
        require('../_common/recently-watched.bemjson.js'),

        require('../_common/footer.bemjson.js')
    ]
};
