module.exports = {
    block: 'page',
    title: 'Текстовая',
    styles: [{elem: 'css', url: '../_merged/_merged.css'},
        {
            elem: 'css',
            url: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;subset=cyrillic-ext'
        }
    ],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../_common/header.bemjson.js')(true),
        {
            block: 'block',
            mods: {theme: 'light'},
            attrs: {
                'data-theme': 'light'
            },
            content: [
                {
                    block: 'container',
                    content: require('../_common/demo/demo.bemjson.js')
                },
            ]
        },

        require('../_common/footer.bemjson.js')
    ]
};
