module.exports = {
    block: 'page',
    title: 'Галерея детально',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../_common/header.bemjson.js')(),

        {
            block: 'block',
            mods: {theme: 'light'},
            attrs: {
                'data-theme': 'light'
            },
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'breadcrumb'
                        },
                        {
                            block: 'title',
                            mods: {first:true},
                            mix: [{block: 'h1'}],
                            tag: 'h1',
                            content: 'PAIN – Зал ожидания'
                        },
                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    mods: {xs: '12'},
                                    content: [
                                        {
                                            block: 'article',
                                            mods: {'blog-detail': true},
                                            content: [
                                                {
                                                    block: 'p',
                                                    content: 'Благодаря организаторам концерта - Rain Embraсe Concert Аgency нам удалось попасть на концертную площадку задолго до открытия входных дверей, и своими глазами увидеть эксклюзивную вещь - саундчек PAIN. Мы сразу обратили внимание, что сам маэстро - Питер Тагтгрен, принимает активное участие в отстройке звука на всех этапах. Он неоднократно подходил к звукорежиссеру, давая ценные указания. Стало понятно, что отличный звук на сегодняшнем концерте нам гарантирован. '
                                                },

                                            ]
                                        },
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'slider-article',
                            content: [
                                {
                                    elem: 'big',
                                    content: [
                                        {
                                            elem: 'item-big',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-article/img-1.jpg'
                                                },
                                            ]
                                        },
                                        {
                                            elem: 'item-big',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-article/img-2.jpg'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'item-big',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-article/img-1.jpg'
                                                },
                                            ]
                                        },
                                        {
                                            elem: 'item-big',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-article/img-2.jpg'
                                                }
                                            ]
                                        },
                                    ]
                                },
                                {
                                    elem: 'nav-wrap',
                                    content: [
                                        {
                                            elem: 'nav-slider',
                                            content: [
                                                {
                                                    elem: 'item-nav',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true},
                                                            url: 'slider-article/img-1_min.jpg'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'item-nav',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true},
                                                            url: 'slider-article/img-2_min.jpg'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'item-nav',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true},
                                                            url: 'slider-article/img-1_min.jpg'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'item-nav',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true},
                                                            url: 'slider-article/img-2_min.jpg'
                                                        }
                                                    ]
                                                },
                                            ]
                                        },
                                        {
                                            elem: 'counter'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'you-read',
                            content: [
                                {
                                    elem: 'title',
                                    content: 'Вы читаете рубрику «Отчеты с концертов»:'
                                },
                                {
                                    elem: 'prev',
                                    content: 'Философия Остина Брюсовича Дикинсона'
                                },
                                {
                                    elem: 'now',
                                    content: 'PAIN Зал Ожидания'
                                },
                                {
                                    elem: 'next',
                                    content: 'Новая музыка Тони Айомми. Необычная и прекрасная'
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        require('../_common/recently-watched.bemjson.js'),
        
        require('../_common/footer.bemjson.js')
    ]
};
