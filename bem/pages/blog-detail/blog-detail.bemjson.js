module.exports = {
    block: 'page',
    title: 'Блог детально',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../_common/header.bemjson.js')(),

        {
            block: 'block',
            mods: {theme: 'light'},
            attrs: {
                'data-theme': 'light'
            },
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'breadcrumb'
                        },
                        {
                            block: 'title',
                            mods: {first:true},
                            mix: [{block: 'h1'}],
                            tag: 'h1',
                            content: 'PAIN – Зал ожидания'
                        },
                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '3', sm: '4'},
                                    content: [
                                        {
                                            block: 'sidebar',
                                            content: [
                                                {
                                                    block: 'rubric',
                                                    content: [
                                                        {
                                                            elem: 'title',
                                                            content: 'Рок-блог'
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: [
                                                                {
                                                                    elem: 'name',
                                                                    content: 'Леонид Фридман рассказывает'
                                                                },
                                                                {
                                                                    elem: 'count',
                                                                    content: '18'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: [
                                                                {
                                                                    elem: 'name',
                                                                    content: 'Мир Castle rock'
                                                                },
                                                                {
                                                                    elem: 'count',
                                                                    content: '17'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: [
                                                                {
                                                                    elem: 'name',
                                                                    content: 'Отчеты с концертов'
                                                                },
                                                                {
                                                                    elem: 'count',
                                                                    content: '26'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: [
                                                                {
                                                                    elem: 'name',
                                                                    content: 'Про рок'
                                                                },
                                                                {
                                                                    elem: 'count',
                                                                    content: '250'
                                                                }
                                                            ]
                                                        },
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '9', sm: '8'},
                                    content: [
                                        {
                                            block: 'blog-detail-head',
                                            content: [
                                                {
                                                    elem: 'date',
                                                    content: '24.12.2016'
                                                },
                                                {
                                                    elem: 'label',
                                                    content: 'Теги:'
                                                },
                                                {
                                                    elem: 'tag-list',
                                                    content: [
                                                        {
                                                            elem: 'tag-item',
                                                            content: 'Disturbed'
                                                        },
                                                        {
                                                            elem: 'tag-item',
                                                            content: 'Twenty One Pilots'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            block: 'quote',
                                            content: [
                                                {
                                                    elem: 'more',
                                                    content: {
                                                        block: 'a',
                                                        content: 'Читайте об этом в нашем блоге'
                                                    }
                                                },
                                                {
                                                    elem: 'block',
                                                    content: [
                                                        {
                                                            block: 'item',
                                                            content: '— Отличное качество, низкие цены! <br/> Спасибо CastleRock!'
                                                        },
                                                        {
                                                            elem: 'name',
                                                            content: 'Nightwish'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            block: 'article',
                                            mods: {'blog-detail': true},
                                            content: [
                                                {
                                                    block: 'p',
                                                    content: 'Благодаря организаторам концерта - Rain Embraсe Concert Аgency нам удалось попасть на концертную площадку задолго до открытия входных дверей, и своими глазами увидеть эксклюзивную вещь - саундчек PAIN. Мы сразу обратили внимание, что сам маэстро - Питер Тагтгрен, принимает активное участие в отстройке звука на всех этапах. Он неоднократно подходил к звукорежиссеру, давая ценные указания. Стало понятно, что отличный звук на сегодняшнем концерте нам гарантирован. '
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Первой выступала разогревающая питерская группа PitchBlack. Ребята очень старались, хотя их музыкальный стиль немного выбивался из формата мероприятия. '
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Заиграла интро-фонограмма (Billy Idol – Rebel Yell), а значит пришло время для хэдлайнера этого вечера, и ровно в девять на сцене появились PAIN. Питер Тагтгрен в своём классическом концертном одеянии – смирительной рубашке с длинными рукавами, за барабанной установкой его сын – Себастиан Тагтгрен (записывал партии ударных к новому альбому PAIN “Coming Home”, который вышел в сентябре этого года), на гитаре – Грегер Андерсон. В связи с личными семейными проблемами бас-гитарист Андре Скауг не смог присоединиться к группе (пропустил оба российских выступления) – его заменял гитарный техник Nightwish/PAIN Анти Тойвиайнен.'
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Благодаря организаторам концерта - Rain Embraсe Concert Аgency нам удалось попасть на концертную площадку задолго до открытия входных дверей, и своими глазами увидеть эксклюзивную вещь - саундчек PAIN. Мы сразу обратили внимание, что сам маэстро - Питер Тагтгрен, принимает активное участие в отстройке звука на всех этапах. Он неоднократно подходил к звукорежиссеру, давая ценные указания. Стало понятно, что отличный звук на сегодняшнем концерте нам гарантирован. '
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Первой выступала разогревающая питерская группа PitchBlack. Ребята очень старались, хотя их музыкальный стиль немного выбивался из формата мероприятия. Заиграла интро-фонограмма (Billy Idol – Rebel Yell), а значит пришло время для хэдлайнера этого вечера, и ровно в девять на сцене появились PAIN. Питер Тагтгрен в своём классическом концертном одеянии – смирительной рубашке с длинными рукавами, за барабанной установкой его сын – Себастиан Тагтгрен (записывал партии ударных к новому альбому PAIN “Coming Home”, который вышел в сентябре этого года), на гитаре – Грегер Андерсон. В связи с личными семейными проблемами бас-гитарист Андре Скауг не смог присоединиться к группе (пропустил оба российских выступления) – его заменял гитарный техник Nightwish/PAIN Анти Тойвиайнен. '
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Заиграла интро-фонограмма (Billy Idol – Rebel Yell), а значит пришло время для хэдлайнера этого вечера, и ровно в девять на сцене появились PAIN. Питер Тагтгрен в своём классическом концертном одеянии – смирительной рубашке с длинными рукавами, за барабанной установкой его сын – Себастиан Тагтгрен (записывал партии ударных к новому альбому PAIN “Coming Home”, который вышел в сентябре этого года), на гитаре – Грегер Андерсон. В связи с личными семейными проблемами бас-гитарист Андре Скауг не смог присоединиться к группе (пропустил оба российских выступления) – его заменял гитарный техник Nightwish/PAIN Анти Тойвиайнен.'
                                                },
                                            ]
                                        },
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'h3',
                            content: 'Галерея концерт PAIN'
                        },
                        {
                            block: 'slider-article',
                            content: [
                                {
                                    elem: 'big',
                                    content: [
                                        {
                                            elem: 'item-big',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-article/img-1.jpg'
                                                },
                                            ]
                                        },
                                        {
                                            elem: 'item-big',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-article/img-2.jpg'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'item-big',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-article/img-1.jpg'
                                                },
                                            ]
                                        },
                                        {
                                            elem: 'item-big',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'slider-article/img-2.jpg'
                                                }
                                            ]
                                        },
                                    ]
                                },
                                {
                                    elem: 'nav-wrap',
                                    content: [
                                        {
                                            elem: 'nav-slider',
                                            content: [
                                                {
                                                    elem: 'item-nav',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true},
                                                            url: 'slider-article/img-1_min.jpg'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'item-nav',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true},
                                                            url: 'slider-article/img-2_min.jpg'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'item-nav',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true},
                                                            url: 'slider-article/img-1_min.jpg'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'item-nav',
                                                    content: [
                                                        {
                                                            block: 'img',
                                                            mods: {lazyload: true},
                                                            url: 'slider-article/img-2_min.jpg'
                                                        }
                                                    ]
                                                },
                                            ]
                                        },
                                        {
                                            elem: 'counter'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'you-read',
                            content: [
                                {
                                    elem: 'title',
                                    content: 'Вы читаете рубрику «Отчеты с концертов»:'
                                },
                                {
                                    elem: 'prev',
                                    content: [
                                        {
                                            elem: 'link',
                                            content: 'Философия Остина Брюсовича Дикинсона'
                                        }
                                    ]
                                },
                                {
                                    elem: 'now',
                                    content: 'PAIN Зал Ожидания'
                                },
                                {
                                    elem: 'next',
                                    content: [
                                        {
                                            elem: 'link',
                                            content: 'Новая музыка Тони Айомми. Необычная и прекрасная'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
            ]
        },
        {
            block: 'block',
            mods: {theme: 'read-more'},
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'h3',
                            mix: [{block: 'mbl'}],
                            content: 'Читайте больше материалов по тегам:'
                        },
                        {
                            block: 'row',
                            mods: {
                                'columns-md': '3',
                                'columns-sm': '2',
                                'columns-xs': '1',
                            },
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6', xs: '12'},
                                    content: [
                                        require('../_common/preview-blog.bemjson.js'),
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6', xs: '12'},
                                    content: [
                                        require('../_common/preview-blog.bemjson.js'),
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6', xs: '12'},
                                    content: [
                                        require('../_common/preview-blog.bemjson.js'),
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6', xs: '12'},
                                    content: [
                                        require('../_common/preview-blog.bemjson.js'),
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6', xs: '12'},
                                    content: [
                                        require('../_common/preview-blog.bemjson.js'),
                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '4', sm: '6', xs: '12'},
                                    content: [
                                        require('../_common/preview-blog.bemjson.js'),
                                    ]
                                },

                            ]
                        }
                    ]
                }
            ]
        },
        require('../_common/recently-watched.bemjson.js'),
        
        require('../_common/footer.bemjson.js')
    ]
};
