module.exports = {
    block: 'page',
    title: 'Каталог',
    styles: [{elem: 'css', url: '../_merged/_merged.css'},
    ],
    scripts: [
        {elem: 'js', content: "function ajaxPopoverSmartFilter(callContext, method, target) {var count = Math.floor(Math.random() * (100 - 0 + 1)) + 0;count = '(' + count + ')';method.call(callContext, target, count);}"},
        {elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../_common/header.bemjson.js')(),

        {
            block: 'block',
            mods: {theme: 'light'},
            attrs: {
                'data-theme': 'light'
            },
            content: [
                {
                    block: 'container',
                    content: [
                        {
                            block: 'breadcrumb'
                        },
                        {
                            block: 'title',
                            mods: {first: true},
                            mix: [{block: 'h1'}],
                            tag: 'h1',
                            content: 'Рок атрибутика и аксессуары'
                        },
                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    mods: {md: '3', sm: '4'},
                                    content: [
                                        {
                                            block: 'sidebar',
                                            mix: [{block: 'hidden-xs'}],
                                            content: [
                                                {
                                                    block: 'sidebar-menu',
                                                    content: [
                                                        {
                                                            elem: 'item',
                                                            content: [
                                                                {
                                                                    elem: 'link',
                                                                    tag: 'a',
                                                                    mods: {active: true},
                                                                    attrs: {
                                                                        href: '#'
                                                                    },
                                                                    content: 'Брюки'
                                                                },
                                                                {
                                                                    elem: 'submenu',
                                                                    content: [
                                                                        'Брюки в клетку',
                                                                        'Брюки кожаные',
                                                                        'Брюки милитари',
                                                                        'Брюки со шнуровкой',
                                                                        'Легинсы'
                                                                    ]
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: 'Галстуки',
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: 'Зеркала'
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: 'Напульсники и фенечки',
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: 'Нашивки',
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: 'Маски',
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: 'Ошейники',
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: 'Ремни',
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: 'Пряжки',
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: 'Тату-рукава',
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: 'Хвосты',
                                                        },
                                                        {
                                                            elem: 'item',
                                                            content: 'Цепи и карабины',
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            block: 'collapse',
                                            mods: {filter: true},
                                            attrs: {
                                                id: 'filter-catalog'
                                            },
                                            content: [
                                                {
                                                    block: 'clearfix',
                                                    mix: [{block: 'mbm'}],
                                                    content: [
                                                        {
                                                            block: 'h4',
                                                            mix: [{block: 'mbx'}, {block: 'pull-left'}],
                                                            content: 'Фильтры'
                                                        },
                                                        {
                                                            block: 'smart-filter-all-reset',
                                                            mix: [{block: 'pull-right'}],
                                                            content: 'Сбросить всё'
                                                        },
                                                    ]
                                                },
                                                {
                                                    block: 'sidebar',
                                                    mods: {'smart-filter': true},
                                                    content: [
                                                        {
                                                            block: 'smart-filter',
                                                            content: [
                                                                {
                                                                    elem: 'item',
                                                                    content: [
                                                                        {
                                                                            elem: 'reset',
                                                                            content: 'Сбросить'
                                                                        },
                                                                        {
                                                                            elem: 'title',
                                                                            content: 'Размеры в наличии:'
                                                                        },
                                                                        {
                                                                            elem: 'list',
                                                                            content: [
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'XS'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'S'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'M'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'XL'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'XXL'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'XXL'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'XXXL'
                                                                                },
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: [
                                                                        {
                                                                            elem: 'reset',
                                                                            content: 'Сбросить'
                                                                        },
                                                                        {
                                                                            elem: 'title',
                                                                            content: 'Тематика'
                                                                        },
                                                                        {
                                                                            elem: 'list',
                                                                            content: [
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'AC/DC'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Aerosmith'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Alice Cooper'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Black Sabbath'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Bon Jovi'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'AC/DC'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Aerosmith'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Slipknot'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Pantera'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Iron Maiden'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Children Of Bodom'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Гражданская оборона'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Disturbed'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Metallica'
                                                                                },

                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: [
                                                                        {
                                                                            elem: 'reset',
                                                                            content: 'Сбросить'
                                                                        },
                                                                        {
                                                                            elem: 'title',
                                                                            content: 'Цена'
                                                                        },
                                                                        {
                                                                            block: 'filter-slider',
                                                                            content: [
                                                                                {
                                                                                    elem: 'label-wrap',
                                                                                    content: [
                                                                                        {
                                                                                            elem: 'label',
                                                                                            content: [
                                                                                                'от',
                                                                                                {
                                                                                                    elem: 'input-min',
                                                                                                    min: '500',
                                                                                                },
                                                                                            ]
                                                                                        },

                                                                                        {
                                                                                            elem: 'label',
                                                                                            content: [
                                                                                                'до',
                                                                                                {
                                                                                                    elem: 'input-max',
                                                                                                    max: '5000',
                                                                                                },
                                                                                            ]
                                                                                        },
                                                                                    ]
                                                                                },


                                                                                {
                                                                                    elem: 'slider'
                                                                                },
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: [
                                                                        {
                                                                            elem: 'reset',
                                                                            content: 'Сбросить'
                                                                        },
                                                                        {
                                                                            elem: 'title',
                                                                            content: 'Материал'
                                                                        },
                                                                        {
                                                                            elem: 'list',
                                                                            content: [
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Метал'
                                                                                },
                                                                                {
                                                                                    block: 'checkbox',
                                                                                    mods: {custom: true, filter: true},
                                                                                    content: 'Кожа'
                                                                                },
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: [
                                                                        {
                                                                            elem: 'title',
                                                                            content: 'Цвет'
                                                                        },
                                                                        {
                                                                            block: 'list-color',
                                                                            content: [
                                                                                {
                                                                                    elem: 'item',
                                                                                    attrs: {
                                                                                        style: 'background-color: #c90b1f'
                                                                                    }
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    attrs: {
                                                                                        style: 'background-color: #f4e22a'
                                                                                    }
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    attrs: {
                                                                                        style: 'background-color: #7eb946'
                                                                                    }
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    attrs: {
                                                                                        style: 'background-color: #00c7c7'
                                                                                    }
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    attrs: {
                                                                                        style: 'background-color: #0000d6'
                                                                                    }
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    attrs: {
                                                                                        style: 'background-color: #c000c0'
                                                                                    }
                                                                                },
                                                                                {
                                                                                    elem: 'item',
                                                                                    attrs: {
                                                                                        style: 'background-image: url(../../../images/smart-filter/color-img.jpg)'
                                                                                    }
                                                                                },

                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    elem: 'popover',
                                                                    content: [
                                                                        {
                                                                            block: 'btn',
                                                                            mods: {color: 'primary'},
                                                                            content: [
                                                                                'Показать',
                                                                                {
                                                                                    elem: 'count',
                                                                                    tag: 'span',
                                                                                    mix: [{block: 'smart-filter', elem: 'count'}]
                                                                                }
                                                                            ]
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        },


                                                    ]
                                                },
                                            ]
                                        },
                                        {
                                            block: 'btn',
                                            mods: {block: true},
                                            mix: [{block: 'visible-xs-inline-block'}, {block: 'mbm'}],
                                            attrs: {
                                                'data-text': 'Скрыть фильтр',
                                                'data-toggle': 'collapse'
                                            },
                                            url: '#filter-catalog',
                                            content: 'Показать фильтр'
                                        },
                                        {
                                            block: 'sidebar-easy',
                                            mix: [{block: 'hidden-xs'}],
                                            content: [
                                                {
                                                    block: 'h6',
                                                    mix: [{block: 'mtm'}],
                                                    content: 'Также смотрите:'
                                                },
                                                {
                                                    block: 'ul',
                                                    mix: [{block: 'list-unstyled'}],
                                                    content: [
                                                        {
                                                            elem: 'link',
                                                            content: 'Брюки'
                                                        },
                                                        {
                                                            elem: 'link',
                                                            content: 'Жилеты'
                                                        },
                                                        {
                                                            elem: 'link',
                                                            content: 'Рубашки'
                                                        },
                                                        {
                                                            elem: 'link',
                                                            content: 'Подарочные сертификаты'
                                                        },
                                                        {
                                                            elem: 'link',
                                                            content: 'Сувениры'
                                                        },
                                                    ]
                                                }
                                            ]
                                        },

                                    ]
                                },
                                {
                                    elem: 'col',
                                    mods: {md: '9', sm: '8'},
                                    content: [
                                        {
                                            block: 'catalog-img-link',
                                            content: [
                                                {
                                                    block: 'img',
                                                    mods: {lazyload: true, responsive: true},
                                                    url: 'catalog/img-big-1.jpg'
                                                }
                                            ]
                                        },
                                        {
                                            block: 'catalog-control',
                                            content: [
                                                {
                                                    elem: 'left',
                                                    content: [
                                                        {
                                                            elem: 'label',
                                                            content: 'Сортировать: '
                                                        },
                                                        {
                                                            elem: 'sorting',
                                                            content: 'Цена по возрастанию <i class="fa fa-caret-up" aria-hidden="true"></i>'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'right',
                                                    content: [
                                                        {
                                                            block: 'pagination',
                                                            mods: {small: true},
                                                            content: [
                                                                {
                                                                    elem: 'prev',
                                                                    content: '<span>Предыдущая</span><i class="fa fa-chevron-left" aria-hidden="true"></i>'
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    mix: [{block: 'active'}],
                                                                    content: '1'
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: '2'
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: '3'
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: '4'
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: '5'
                                                                },
                                                                {
                                                                    elem: 'next',
                                                                    content: '<span>Следующая</span><i class="fa fa-chevron-right" aria-hidden="true"></i>'
                                                                },
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            block: 'row',
                                            mods: {
                                                card: true,
                                                catalog: true,
                                                'columns-lg': 4,
                                                'columns-md': 4,
                                                'columns-sm': 2,
                                                'columns-xs': 2
                                            },
                                            content: [
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            new: true,
                                                            img: 'card/card-2-1.jpg',
                                                            img2: 'card/card-2-2.jpg',
                                                            bonus: '+ 199 баллов',
                                                            title: 'Майка Black Veil Brides',
                                                            price: '790'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-3-1.jpg',
                                                            img2: 'card/card-3-2.jpg',
                                                            title: 'Жилет кожаный First женский'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-4-1.jpg',
                                                            img2: 'card/card-4-2.jpg',
                                                            title: 'Жилет кожаный First женский lack Veil Brides Брюки в клетку Красная крупная'
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-5-1.jpg',
                                                            img2: 'card/card-5-2.jpg',
                                                            bonus: '+ 199 баллов',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            new: true,
                                                            img: 'card/card-3-1.jpg',
                                                            img2: 'card/card-3-2.jpg',
                                                            bonus: '+ 199 баллов',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            bonus: '+ 199 баллов',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-4-1.jpg',
                                                            img2: 'card/card-4-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-2-1.jpg',
                                                            img2: 'card/card-2-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            new: true,
                                                            img: 'card/card-4-1.jpg',
                                                            img2: 'card/card-4-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-2-1.jpg',
                                                            img2: 'card/card-2-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            new: true,
                                                            img: 'card/card-5-1.jpg',
                                                            img2: 'card/card-5-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-5-1.jpg',
                                                            img2: 'card/card-5-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-5-1.jpg',
                                                            img2: 'card/card-5-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-4-1.jpg',
                                                            img2: 'card/card-4-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-2-1.jpg',
                                                            img2: 'card/card-2-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            new: true,
                                                            img: 'card/card-4-1.jpg',
                                                            img2: 'card/card-4-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-2-1.jpg',
                                                            img2: 'card/card-2-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            new: true,
                                                            img: 'card/card-5-1.jpg',
                                                            img2: 'card/card-5-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-5-1.jpg',
                                                            img2: 'card/card-5-2.jpg',
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    mods: {lg: '3', md: '3', xs: '6'},
                                                    content: [
                                                        {
                                                            block: 'card',
                                                            mods: {'catalog': true},
                                                            img: 'card/card-5-1.jpg',
                                                            img2: 'card/card-5-2.jpg',
                                                        }
                                                    ]
                                                },
                                            ]
                                        },
                                        {
                                            block: 'catalog-control',
                                            mix: [{block: 'mbn'}],
                                            content: [
                                                {
                                                    elem: 'right',
                                                    content: [
                                                        {
                                                            block: 'pagination',
                                                            mods: {small: true},
                                                            content: [
                                                                {
                                                                    elem: 'prev',
                                                                    content: '<span>Предыдущая</span><i class="fa fa-chevron-left" aria-hidden="true"></i>'
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    mix: [{block: 'active'}],
                                                                    content: '1'
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: '2'
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: '3'
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: '4'
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: '5'
                                                                },
                                                                {
                                                                    elem: 'next',
                                                                    content: '<span>Следующая</span><i class="fa fa-chevron-right" aria-hidden="true"></i>'
                                                                },
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            block: 'text-center',
                                            mix: [{block: 'ptm'}],
                                            content: [
                                                {
                                                    block: 'btn',
                                                    mods: {color: 'info', size: 'lg'},
                                                    mix: [{block: 'mbl'}],
                                                    content: 'Показать ещё'
                                                }
                                            ]
                                        },
                                        {
                                            block: 'article',
                                            mods: {catalog: true},
                                            content: [
                                                {
                                                    elem: 'title',
                                                    content: 'Лучшие рок-футболки'
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Кожаная одежда и обувь – основные, но не главные элементы рокерского прикида. Древние говорили: «Дьявол кроется в мелочах». Это верно – гардероб создают аксессуары. Именно они отличают рокера от любого другого чувака в косухе. И только они помогут сделать твой рок-имидж законченным и полным. Выбор рок-аксессуаров в магазине Кастл Рок поистине огромен. Есть где разгуляться твоей богатой фантазии!'
                                                },
                                                {
                                                    block: 'p',
                                                    content: 'Главное при этом – чувство меры. Ведь аксессуары замечательны тем, что их можно и нужно комбинировать в различных сочетаниях. И тогда ты каждый день будешь выглядеть по-новому. Кастл Рок торгует исключительно стильной рок-атрибутикой, яркой и незабываемой. Поэтому любой твой выбор будет удачным. Ты можешь купить нашивку с любимой рок-группой, и это будет уникальная нашивка. Или тяжелый кожаный напульсник с металлическими шипами, точно такой, какие ты видел на запястьях многих рок- музыкантов...'
                                                },
                                                {
                                                    block: 'collapse',
                                                    attrs: {
                                                        id: 'article-main-collapse'
                                                    },
                                                    content: [
                                                        {
                                                            tag: 'p',
                                                            content: 'Это самый неформальный магазин из всех. Castle Rock – это легенда, культовый магазин для рокеров уже больше двадцати лет. Старейший в России и крупнейший в Европе, место паломничества и общения единомышленников. У нас огромный ассортимент – более 20 000 наименований рок-атрибутики и неформальной одежды: косухи, кожаные плащи и штаны, пряжки, уникальные нашивки, банданы, напульсники и ремни с заклепками и шипами, тысячи видов футболок и толстовок с любимыми рок-группами, гады, берцы, казаки, готические платья и корсеты, множество перстней и подвесов, серьги и косметички, книги и журналы, и даже товары для детей. И конечно, рок-музыка. Но главное в Castle Rock – это единый, уникальный рок стиль, присущий каждой вещи, которая здесь продается. С пустыми руками от нас не уходят, каждый найдет себе товар по душе.'
                                                        },
                                                        {
                                                            tag: 'p',
                                                            content: 'Можно ли купить рок? Ни за что. Рок не продается. Он всегда был и останется самым независимым направлением музыки. Бунтарство и протест деньгами не измерить. Но можно купить то, что с роком связано, напрямую или опосредованно, через различные предметы и вещи с рок-символикой. И делать это лучше всего в особом рок-магазине. Вот он, прямо перед тобой, смотри – ты уже в нем! Добро пожаловать в интернет-магазин рок-атрибутики Castle Rock!'
                                                        },
                                                    ]
                                                },
                                                {
                                                    block: 'readmore-link',
                                                    attrs: {
                                                        'data-toggle': 'collapse',
                                                        'data-target': '#article-main-collapse',
                                                        'data-text': 'Свернуть'
                                                    },
                                                    content: 'Читать далее'
                                                },
                                            ]
                                        },
                                    ]
                                }
                            ]
                        },

                    ]
                }
            ]
        },
        require('../_common/recently-watched.bemjson.js'),

        require('../_common/footer.bemjson.js'),

        require('../_common/quick-view.bemjson.js')

    ]
};
