$(function () {
    var checkbox = $('.checkbox');

    checkbox.each(toggleClassChecked);

    checkbox.on('change', toggleClassChecked)
    
    function toggleClassChecked() {
        var $this = $(this);
        var input = $this.find('.checkbox__control');
        if (input.prop('checked')) {
            $this.addClass('checkbox_checked');
        } else {
            $this.removeClass('checkbox_checked');
        }
    }
})