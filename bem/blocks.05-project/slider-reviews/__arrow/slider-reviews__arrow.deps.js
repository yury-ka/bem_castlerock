[
    ({
        mustDeps: []
    }),
    ({
        shouldDeps: [
            {mods: ['left', 'right']}
        ]
    })
]