module.exports = function (bh) {
    bh.match('collection-list', function (ctx, json) {
        var content;

        if (typeof ctx.content()[0] === 'string') {
            content = ctx.content().map(function (item, i) {
                var tempItem;
                if (i == 40 || i == 52) {
                    tempItem = [
                        {
                            elem: 'li',
                            tag: 'li',
                            mods: {primary: true},
                            content: {
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                content: item
                            }
                        }
                    ]
                } else {
                    tempItem = [
                        {
                            elem: 'li',
                            tag: 'li',
                            content: {
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                content: item
                            }
                        }
                    ]
                }
                return tempItem
            });
        }

        ctx.content([
            {
                elem: 'title',
                content: json.title
            },
            {
                elem: 'ul',
                tag: 'ul',
                content: content
            }
            ],true)
    })
};