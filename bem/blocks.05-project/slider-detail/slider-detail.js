window.iv = window.iv || {};
window.iv.ui = window.iv.ui || {};

window.iv.ui.sliderDetail = {
    _dom: {},
    init: function () {
        this._dom = {
            slider: $('.slider-detail__big'),
            nav: $('.slider-detail__nav'),
            img: $('.slider-detail__img'),
            wrapForZoom: $('#product-control_detail')
        }
        var self = this;

        this._dom.slider.on('init', function(slick){
            if ($('html').hasClass('no-touch')) {
                self.initZoom();
            }
        });

        self.initSlider();
        self.initNav();

    },
    initSlider: function () {
        this._dom.slider.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-detail__nav',
        });
    },
    initNav: function () {
        this._dom.nav.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            vertical: true,
            asNavFor: '.slider-detail__big',
            dots: false,
            centerMode: false,
            focusOnSelect: true
        });
    },
    initZoom: function () {
        var wrapForZoom = this._dom.wrapForZoom;
        var modalQuickView = $('#quick-view-modal');

        var hImg, wImg;
        var self = this;

        if(modalQuickView.length) {
            modalQuickView.on('shown.bs.modal', function (e) {
                recalculateSize();
                console.log('есть модальное окно');
                FuncinitZoom();
            })
        } else {
            recalculateSize();
            FuncinitZoom();
        }

        function recalculateSize() {
            hImg = wrapForZoom.outerHeight();
            wImg = wrapForZoom.outerWidth();
        }
        function FuncinitZoom() {
            self._dom.img.elevateZoom({
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 500,
                lensFadeIn: 500,
                lensFadeOut: 500,
                zoomWindowPosition: "product-control_detail",
                borderSize: 0,
                easing: true,
                zoomWindowWidth: wImg,
                zoomWindowHeight: hImg,
                responsive: true,
            });
        }

    }
}

$(function () {
    window.iv.ui.sliderDetail.init();
})
