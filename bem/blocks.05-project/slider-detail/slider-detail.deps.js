[
    ({
        mustDeps: [
            {block: 'slick-carousel'},
            {block: 'lightbox2'},
            {block: 'zoomsl'}
        ]
    }),
    ({
        shouldDeps: []
    })
]