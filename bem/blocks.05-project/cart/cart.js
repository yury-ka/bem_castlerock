window.iv = window.iv || {};
window.iv.ui = window.iv.ui || {};


$(function () {
    var list =  $('.cart__list'),
        dropdon = $('.cart');

    list.perfectScrollbar();

    dropdon.on('shown.bs.dropdown', function () {
        list.perfectScrollbar('update');
    })

    window.iv.ui.updateCartScroll = function () {
        list.perfectScrollbar('update');
        console.log(list);
    }
})