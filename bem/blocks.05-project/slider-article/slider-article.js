window.iv = window.iv || {};
window.iv.ui = window.iv.ui || {};

window.iv.ui.sliderArticle = {
    _dom: {},
    init: function () {
        this._dom = {
            slider: $('.slider-article__big'),
            nav: $('.slider-article__nav-slider'),
            img: $('.slider-article__img'),
            counter: $('.slider-article__counter')
        }
        var self = this;

        this._dom.slider.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
            //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
            var i = (currentSlide ? currentSlide : 0) + 1;
            self._dom.counter.html('<span>' + i + '</span>/' + slick.slideCount);
        });

        self.initSlider();
        self.initNav();

    },
    initSlider: function () {
        this._dom.slider.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            centerMode: true,
            asNavFor: '.slider-article__nav-slider',
        });
    },
    initNav: function () {
        this._dom.nav.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            vertical: true,
            asNavFor: '.slider-article__big',
            dots: false,
            centerMode: false,
            adaptiveHeight: true,
            focusOnSelect: true,
            prevArrow: '<button type="button" class="slider-article__arrow slider-article__arrow_for_prev"><img src="../../../images/slider-article/arrow-prev.png" alt=""></button>',
            nextArrow: '<button type="button" class="slider-article__arrow slider-article__arrow_for_next"><img src="../../../images/slider-article/arrow-next.png" alt=""></button>'
        });
    }
}

$(function () {
    window.iv.ui.sliderArticle.init();
})

