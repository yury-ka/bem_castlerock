[
    ({
        mustDeps: [
            {block: 'slick-carousel'}
        ]
    }),
    ({
        shouldDeps: [
            {elem: ['arrow']}
        ]
    })
]