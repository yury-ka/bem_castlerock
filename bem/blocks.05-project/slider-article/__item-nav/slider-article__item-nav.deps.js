[
    ({
        mustDeps: [
            {block: 'slick-carousel'}
        ]
    }),
    ({
        shouldDeps: [
            {mod: 'active'}
        ]
    })
]