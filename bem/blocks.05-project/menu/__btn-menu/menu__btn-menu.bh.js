module.exports = function (bh) {
    bh.match('menu__btn-menu', function (ctx, json) {
        ctx.tag('button')
            .attrs({
                'data-toggle': 'collapse',
                'data-target': '#menu'
            })
    })
};