$(function () {
    var card = $('.card');

    card.on('touchstart', function (e) {
        var $this = $(this);
        var lister = window.iv.api.media.getKey();
        if (lister === 'xs' || lister === 'ex') return;
        card.removeClass('card_active');
        setTimeout(function () {
            card.find('.card__link-img').hide();
        }, 450)

        $this.addClass('card_active');
        setTimeout(function () {
            $this.find('.card__link-img').show();
        }, 500)
    });

    if($('html').hasClass('touch')) {
        $(document).on('touchstart', function (event) {
            var $this = $(this);
            var $target = $(event.target);

            if (!$target.closest('.card').length) {

                card.removeClass('card_active');
                card.find('.card__link-img').hide();
            }
        })
    }

})