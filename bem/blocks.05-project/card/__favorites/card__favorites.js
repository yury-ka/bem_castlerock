window.iv = window.iv || {};
window.iv.ui = window.iv.ui || {};

window.iv.ui.cardFavorites = {
    _dom: {},
    init: function () {
        this._dom = {
            btn: $('.card__favorites')
        }
        var self = this,
            btn = this._dom.btn;
        btn.data('init', true);
        btn.attr('init', true);
        btn.click(this.handler);

    },
    reInit: function () {
         if(this._dom.btn.data('init')) {
             this._dom.btn.click(this.handler)
         }
    },
    handler: function () {
        var $this = $(this);
        if ($this.hasClass('card__favorites_active')) {
            $this.removeClass('card__favorites_active');
            $this.trigger('iv.card-favorites.remove');
            $this.addClass('card__favorites_removing');
            setTimeout(function () {
                $this.removeClass('card__favorites_removing');
            }, 1000)
        } else {
            $this.addClass('card__favorites_active');
            $this.trigger('iv.card-favorites.add');
        }
    }

}

$(function () {

    window.iv.ui.cardFavorites.init();

})