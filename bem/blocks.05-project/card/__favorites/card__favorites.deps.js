[
    ({
        mustDeps: []
    }),
    ({
        shouldDeps: [
            {mods: ['active', 'removing']}
        ]
    })
]