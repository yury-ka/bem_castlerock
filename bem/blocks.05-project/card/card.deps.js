[
    ({
        mustDeps: [
            {block: 'dotdotdot'}
        ]
    }),
    ({
        shouldDeps: [
            {elem: [
                'top', 'img', 'price', 'bottom', 'title-link', 'caption', 'size-group', 'size', 'in-stock', 'bonus',
                'img-reverse', 'quick-view', 'quick-view-text', 'mid', 'new', 'favorites', 'link-img'
            ]},
            {mod: 'active'}
        ]
    })
]