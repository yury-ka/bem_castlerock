module.exports = function (bh) {
    bh.match('card__quick-view', function (ctx, json) {
        ctx.tag('a')
            .attrs({
                href: '#',
                'data-toggle':'modal',
                'data-target': '#quick-view-modal'
            })
    })
};