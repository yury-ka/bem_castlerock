module.exports = function (bh) {
    bh.match('card', function (ctx, json) {
        var img = json.img || 'card/card-1-1.jpg',
            img2 = json.img2 || 'card/card-1-2.jpg',
            title = json.title || 'Платье Amy Monton №7',
            price = json.price || '1 300';
        var bonus,
            newProduct;
        
        if (json.bonus) {
            bonus = {
                elem: 'bonus',
                content: '<span>' + json.bonus+ '</span>'
            }
        }
        if (json.new) {
            newProduct = {
                elem: 'new'
            }
        }
        ctx.content([
            {
                elem: 'top',
                content: [
                    newProduct,
                    {
                        block: 'img',
                        mods: {lazyload:true},
                        mix: [{block: 'card', elem: 'img'}],
                        url: img
                    },
                    {
                        block: 'img',
                        mods: {lazyload:true},
                        mix: [{block: 'card', elem: 'img-reverse'}],
                        url: img2
                    },
                    {
                        elem: 'favorites',
                        tag:'button',
                        attrs: {
                            type: 'button'
                        }
                    },
                    {
                        elem: 'link-img',
                        tag: 'a',
                        attrs: {
                            href: '#'
                        }
                    },
                    {
                        elem: 'quick-view',
                        content: {
                            elem: 'quick-view-text',
                            content: 'Быстрый <br/>просмотр'
                        }
                    }
                ]
            },
            {
                elem: 'mid',
                content: [
                    {
                        elem: 'caption',
                        content: [
                            {
                                elem: 'title-link',
                                tag: 'a',
                                attrs: {
                                    href: '#',
                                    alt: title,
                                    'data-dotdotdot': 'true',
                                },
                                content: title
                            },
                        ]
                    },
                    {
                        elem: 'price',
                        content: price + '<span> ₽</span>'
                    },
                    bonus,
                ]
            },
            {
                elem: 'bottom',
                content: [
                    {
                        elem: 'size-group',
                        content: [
                            {
                                elem: 'in-stock',
                                content: 'в наличии:'
                            },
                            {
                                elem: 'size',
                                content: 'S'
                            },
                            {
                                elem: 'size',
                                content: 'M'
                            },
                            {
                                elem: 'size',
                                content: 'L'
                            },
                            {
                                elem: 'size',
                                content: 'XXl'
                            },

                        ]
                    }
                ]
            }
            
        ], true)
    })
};