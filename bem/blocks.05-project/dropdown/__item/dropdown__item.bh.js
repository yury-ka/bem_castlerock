module.exports = function (bh) {
    bh.match('dropdown__item', function (ctx, json) {
        ctx.content([
            {
                elem: 'link',
                tag: 'a',
                attrs: {
                    href: '#'
                },
                content: ctx.content()
            }
        ], true)
    })
};