module.exports = function (bh) {
    bh.match(module.id.replace('.bh.js', '').match(/([^\\]+)?$/), function (ctx, json) {
        ctx.tag('a')
            .content([
                {
                    block: 'img',
                    mix: [{block: 'card-compact', elem: 'img'}],
                    mods: {lazyload:true},
                    url: json.img
                },
                {
                    elem: 'title',
                    content: json.title
                }
            ], true)
    })
};