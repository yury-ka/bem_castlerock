window.iv = window.iv || {};
window.iv.ui = window.iv.ui || {};

window.iv.ui.sidebar = {
    _dom: {},
    init: function () {
        this._dom = {
            btn: $('.btn-side'),
            sidebar: $('.sidebar-flying'),
            close: $('.sidebar-flying__close'),
            body: $('body')
        };
        var self = this;
        var touchClick = window.iv.ui.touchClick();
        this._dom.btn.on(touchClick, function () {
            self.show();
        })
        this._dom.close.on(touchClick, function () {
            self.hide();
        })
    },
    show: function () {
        this._dom.sidebar.addClass('sidebar-flying_active');
        this._dom.body.css('overflow', 'hidden')
    },
    hide: function () {
        this._dom.sidebar.removeClass('sidebar-flying_active');
        this._dom.body.css('overflow', '')
    }
}
$(function () {
    window.iv.ui.sidebar.init();
})  