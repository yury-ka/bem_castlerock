module.exports = function (bh) {
    bh.match(module.id.replace('.bh.js', '').match(/([^\\]+)?$/), function (ctx, json) {
        ctx.tag('label');
        var name = json.name || '';
        var value = json.value || '';
        var checked = json.checked || false;
        
        ctx.content([
            {
                elem: 'input',
                tag: 'input',
                attrs: {
                    type: 'checkbox',
                    name: name,
                    checked : checked,
                    disabled : ctx.mod('disabled'),
                    value: value
                }
            },
        ], true);
    })
};