$(function () {
    var checkbox = $('.list-color__item');

    checkbox.each(toggleClassChecked);

    checkbox.on('change', toggleClassChecked)

    function toggleClassChecked() {
        var $this = $(this);
        var input = $this.find('.list-color__input');
        if (input.prop('checked')) {
            $this.addClass('list-color__item_checked');
        } else {
            $this.removeClass('list-color__item_checked');
        }
    }
});