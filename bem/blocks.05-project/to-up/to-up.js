$(function () {
    var logo = $('.to-up'),
        bg = $('[data-theme]');

    $(window).on('load', function () {
        var logoRecalc = function () {
            var logoOffsetBottom = logo.offset().top + logo.outerHeight(),
                logoOffsetTop = logo.offset().top;
            bg.each(function () {
                var $this = $(this);
                // var top = this.getBoundingClientRect().top;
                var top = $this.offset().top,
                    bottom = $this.offset().top + $this.outerHeight();

                if (top < logoOffsetTop && bottom > logoOffsetTop) {
                    var theme = $this.data('theme');
                   if (theme === 'dark') {
                       logo.addClass('to-up_active')
                   } else {
                       logo.removeClass('to-up_active')
                   }
                }
            })

        };
        logoRecalc();
        window.addEventListener('scroll', logoRecalc);
    });
    logo.on('click', function (e) {
        $('html, body').stop().animate({
            scrollTop: $('body').offset().top
        }, 500);
        e.preventDefault();
    })

})
