module.exports = function (bh) {
    bh.match('to-up', function (ctx, json) {
        ctx.tag('a')
            .attrs({
                href: '#'
            })
            .content([
            {
                elem: 'logo',
                mods: {light:true},
            },
            {
                elem: 'logo',
                mods: {dark:true},
            },
        ], true)
    })
};