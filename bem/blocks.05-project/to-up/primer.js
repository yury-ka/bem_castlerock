
window.addEventListener('load', function() {
    // заполнение блоками
    for (var c = 1; c < 10; c++) {
        var colors = ['white', 'black'];

        var
            block = document.createElement('div');
        block.className = 'block block-' + colors[c%2];
        block.setAttribute('data-color', colors[c%2]);
        block.setAttribute('data-color-rev', colors[1-c%2]);

        document.querySelector('.wrap').appendChild(block);
    }

    // логотипы и их данные.
    var logo = {
        black:  document.querySelector('.logo-black'),
        white:  document.querySelector('.logo-white'),
    };

    logo.offset = logo.black.getBoundingClientRect();

    // ф-я пересчёта логотипа.
    var logoRecalc = function() {
        var bg = {
            top:    document.elementFromPoint(logo.offset.left - 1, logo.offset.top),
            bottom: document.elementFromPoint(logo.offset.left - 1, logo.offset.top + logo.offset.height - 1)
        };

        if (bg.top.getAttribute('data-color') != bg.bottom.getAttribute('data-color')) {
            var offset = bg.bottom.getBoundingClientRect();

            logo_height = {
                top:    offset.top - logo.offset.top,
                bottom: logo.offset.bottom - offset.top
            };

            // определяем соотношение логотипов.
            var
                logo_top = logo[bg.top.getAttribute('data-color-rev')];
            logo_top.style.backgroundPosition = 'top center';
            logo_top.style.height = logo_height.top + 'px';

            var
                logo_bottom = logo[bg.bottom.getAttribute('data-color-rev')];
            logo_bottom.style.backgroundPosition = 'bottom center';
            logo_bottom.style.marginTop = (logo.offset.top + logo_height.top) + 'px';
            logo_bottom.style.height = logo_height.bottom + 'px';
        }
        else {
            // показываем один логотип, скрываем другой.
            var
                logo_top = logo[bg.top.getAttribute('data-color-rev')];
            logo_top.style.marginTop = logo.offset.top + 'px';
            logo_top.style.height = logo.offset.height + 'px';

            var
                logo_hidden = logo[bg.top.getAttribute('data-color')];
            logo_hidden.style.marginTop = logo.offset.top + 'px';
            logo_hidden.style.height = '0px';
        }
    };

    logoRecalc();
    window.addEventListener('scroll', logoRecalc);
});
    