[
    ({
        mustDeps: []
    }),
    ({
        shouldDeps: [
            {elem: ['logo']},
            {mods: ['active']}
        ]
    })
]