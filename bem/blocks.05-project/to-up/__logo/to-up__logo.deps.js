[
    ({
        mustDeps: []
    }),
    ({
        shouldDeps: [
            {mods: ['dark', 'light']}
        ]
    })
]