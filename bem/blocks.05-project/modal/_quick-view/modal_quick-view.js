window.iv = window.iv || {};
window.iv.ui = window.iv.ui || {};

window.iv.ui.reloadSliderModal = function() {
    var big = $('.slider-detail__big'),
        nav = $('.slider-detail__nav');
    big.slick('setPosition');
    nav.slick('setPosition');
    nav.slick('setPosition');
}

$(function () {
    var modal = $('.modal_quick-view');
    modal.on('shown.bs.modal', window.iv.ui.reloadSliderModal)

})