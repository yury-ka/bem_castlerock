$(function () {
    var slider = $('.slider-main');

    var speed = slider.data('speed');

    slider.slick({
        centerMode: true,
        autoplay: true,
        autoplaySpeed: speed || 3000,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        focusOnSelect: true,
        variableWidth: true,
        prevArrow: '<button type="button" class="slider-main__arrow slider-main__arrow_left"></button>',
        nextArrow: '<button type="button" class="slider-main__arrow slider-main__arrow_right"></button>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    centerMode: false,
                    variableWidth: false,
                }
            },
        ]
    })
})