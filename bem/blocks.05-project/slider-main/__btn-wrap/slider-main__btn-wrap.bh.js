module.exports = function (bh) {
    bh.match('slider-main__btn-wrap', function (ctx, json) {
        ctx.content([
            {
                block: 'btn',
                mix: [{block: 'slider-main', elem: 'btn'}],
                content: ctx.content()
            }
        ], true)
    })
};