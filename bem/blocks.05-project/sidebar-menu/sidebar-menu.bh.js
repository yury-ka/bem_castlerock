module.exports = function (bh) {
    bh.match(module.id.replace('.bh.js', '').match(/([^\\]+)?$/), function (ctx, json) {

        ctx.tag('ul');

        if (typeof ctx.content()[0] === 'string') {
            var content = ctx.content().map(function (item) {
                return [
                    {
                        elem: 'item',
                        tag: 'li',
                        content: [
                            {
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                content: item
                            }
                        ]
                    }
                ]
            })
            ctx.content(content, true)
        }

        
    })
};