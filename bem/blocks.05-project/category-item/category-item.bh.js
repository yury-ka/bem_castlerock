module.exports = function (bh) {
    bh.match(module.id.replace('.bh.js', '').match(/([^\\]+)?$/), function (ctx, json) {
        var img = json.img || 'category-item/img-1.jpg',
            title = json.title || 'Галстуки, боло';

        ctx.tag('a')
            .attrs({
                href: '#'
            })
            .content([
                {
                    block: 'img',
                    mix: [{block: 'category-item', elem: 'img'}],
                    mods: {lazyload: true},
                    url: img
                },
                {
                    elem: 'title',
                    content: title
                }
            ], true)
    })
};