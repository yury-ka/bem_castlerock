module.exports = function (bh) {
    bh.match(module.id.replace('.bh.js', '').match(/([^\\]+)?$/), function (ctx, json) {
        ctx.content([
            json.src && {elem: 'img', src: json.src},
            {elem: 'title', content: ctx.content()}
        ], true)
    });
};