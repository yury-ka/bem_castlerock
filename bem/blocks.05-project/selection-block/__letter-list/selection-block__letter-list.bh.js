module.exports = function (bh) {
    bh.match(module.id.replace('.bh.js', '').match(/([^\\]+)?$/), function (ctx, json) {
        ctx.tag('ul')
        var contentArr;
        var varA = [
            'A Day To Remember',
            'AC/DC',
            'Accept',
            'Aerosmith',
            'Amon Amarth',
            'Anthrax',
            'Arch Enemy',
            'Arctic  Monkeys',
            'Asking Alexandria',
            'Arch Enemy Avenged ',
            'Sevenfold',
            'Arctic Monkeys',
            'Asking Alexandria',
        ];
        var varB = [
            'Beatles',
            'Behemoth',
            'Black Sabbath',
            'Black Veil Brides',
            'Blink-182',
            'Bob Marley',
            'Bring Me The Horizon',
            'Bullet For My Valentine',
            'Beatles',
            'Behemoth',
            'Black Sabbath',
            'Black Veil Brides',
            'Blink-182',
            'Bob Marley',
            'Bring Me The Horizon',
            'Bullet For My Valentine',
            'Beatles',
            'Behemoth',
        ]

        switch (json.variant) {
            case 'A':
                contentArr = varA;
                break;
            case 'B':
                contentArr = varB;
                break;
            default:
                contentArr = varA;
        }
        
            var content = contentArr.map(function (item) {
                return [
                    {
                        elem: 'letter-item',
                        tag: 'li',
                        content: [
                            {
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                content: item
                            }
                        ]
                    }
                ]
            })
            ctx.content(content, true)


    })
};