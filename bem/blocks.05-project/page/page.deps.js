([
    {
        mustDeps: [
            {block: 'bootstrap'},
        ]
    },
    {
        shouldDeps: [
            {block: 'body'},
            {block: 'fa'},
            {block: 'custom-fonts'},
            {block: '$js-grid-point'},
            {block: 'link'},
            {block: 'spaces'},
            {block: 'type'},
            {block: 'jquery-eqheight'},
            {block: 'to-up'},
            {block: 'throttle-debounce'},
            {block: 'type'}
        ]
    }
])
