$(function () {
    var searchIsShow = false;

    var search = $('.search_header'),
        input = search.find('.search__input'),
        btn = $('.btn-search');
    btn.click(function (e) {
        e.preventDefault();
        if (!searchIsShow) {
            search.slideDown(300);
            input.focus();
            setTimeout(function () {
                searchIsShow = true;
            }, 200)

        } else {
            search.slideUp(300);
            setTimeout(function () {
                searchIsShow = false;
            }, 200)
        }

    });
    
    $(document).on('click', function (e) {

        if (!search.is(e.target) && search.has(e.target).length === 0) {
            if (searchIsShow){
                search.slideUp(300);
                setTimeout(function () {
                    searchIsShow = false;
                }, 200)
            }
        }
    })


    var lister = window.iv.api.media.getListener();
    lister.xs.addListener(function (media) {
        if (media.matches) {
            search.hide();
        } else {
            search.show();
        }
    });
})