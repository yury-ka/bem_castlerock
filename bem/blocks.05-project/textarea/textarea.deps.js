[
    ({
        mustDeps: [
            {block: 'bootstrap'},
            {block: 'type'},
            {block: 'form-control'}
        ]
    }),
    ({
        shouldDeps: []
    })
]