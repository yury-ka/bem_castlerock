[
    ({
        mustDeps: [
            {block: 'readmore-js'},
            {block: 'dotdotdot'},
            {block: 'bootstrap'}
        ]
    }),
    ({
        shouldDeps: [
            {elem: ['more-link', 'show']}
        ]
    })
]