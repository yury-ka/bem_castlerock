$(function () {
    var slider = $('.slider-blog');

    slider.slick({
        speed: 400,
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,
        focusOnSelect: true,
        prevArrow: '<button type="button" class="slider-blog__arrow slider-blog__arrow_left"></button>',
        nextArrow: '<button type="button" class="slider-blog__arrow slider-blog__arrow_right"></button>',
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    })
})