$(function () {
    var logo = $('.logo_auto-title'),
        logoTitle = logo.find('.logo__title');
    var length = AUTO_TITLE.length;
    var prevNumb = null;
    var showTitle = function () {
        var numb = randomInteger(0, length);
        if (numb !== prevNumb) {
            prevNumb = numb;
            logoTitle.text(AUTO_TITLE[numb]);
        } else {
            showTitle();
        }
    }
    showTitle();
    logo.mouseenter(showTitle)
})

function randomInteger(min, max) {
    var rand = Math.abs(min - 0.5 + Math.random() * (max - min + 1))
    rand = Math.round(rand);
    return rand;
}
