module.exports = function (bh) {
    bh.match('logo', function (ctx, json) {
        var script;
        var title = json.title || 'Легендарный рок-магазин с двадцатилетней историей';
        if (ctx.mod('auto-title')) {
            script = '<script data-move="false">var AUTO_TITLE = ["Легендарный рок-магазин с двадцатилетней историей", "Старейший культовый рок-магазин", "Все товары в наличии! Отправка в день заказа!", "безопасная и удобная оплата", "Доставка по всей России"];</script>'
        } else {

        }
        ctx.tag('a')
            .attrs({
                href: '#'
            })
            .content([
                {
                    elem: 'img',
                    tag: 'img',
                    attrs: {
                        src: ('../../../images/logo.png')
                    }
                },
                {
                    elem: 'title',
                    content: title
                },
                script
            ])
    })
};