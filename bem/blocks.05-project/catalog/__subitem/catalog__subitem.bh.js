module.exports = function (bh) {
    bh.match('catalog__subitem', function (ctx, json) {
        ctx.tag('li')
            .content([
                {
                    elem: 'sublink',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: ctx.content()
                }
            ], true)
    })
};