module.exports = function (bh) {
    bh.match('catalog__link', function (ctx, json) {
        ctx.tag('a')
            .attrs({
                href: '#'
            })
    })
};