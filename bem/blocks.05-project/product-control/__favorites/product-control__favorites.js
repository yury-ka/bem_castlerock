window.iv = window.iv || {};
window.iv.ui = window.iv.ui || {};

window.iv.ui.productFavorites = {
    _dom: {},
    init: function () {
        this._dom = {
            btn: $('.product-control__favorites')
        }
        var self = this,
            btn = this._dom.btn;
        btn.data('init', true);

        btn.click(this.handler)
    },
    reInit: function () {
        if(this._dom.btn.data('init')) {
            this._dom.btn.click(this.handler)
        }
    },
    handler: function () {
        var $this = $(this);
        if ($this.hasClass('product-control__favorites_active')) {
            $this.removeClass('product-control__favorites_active');
            $this.trigger('iv.product-favorites.remove');
        } else {
            $this.addClass('product-control__favorites_active');
            $this.trigger('iv.product-favorites.add');
        }
    }

}

$(function () {

    window.iv.ui.productFavorites.init();

})