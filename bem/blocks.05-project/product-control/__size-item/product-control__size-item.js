$(function () {
    var radio = $('.product-control__size-item');

    radio.each(toggleClassChecked);

    radio.on('change', toggleClassChecked)

    function toggleClassChecked() {
        var $this = $(this);
        var input = $this.find('input');
        radio.removeClass('product-control__size-item_active');
        $this.addClass('product-control__size-item_active');
    }
})