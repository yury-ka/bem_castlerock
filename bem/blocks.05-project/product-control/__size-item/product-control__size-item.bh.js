module.exports = function (bh) {
    bh.match('product-control__size-item', function (ctx, json) {
        ctx.tag('label')
            .content([
                {
                    elem: 'size-radio',
                    tag: 'input',
                    attrs: {
                        type: 'radio',
                        name: 'size-radio'
                    }
                },
                ctx.content()
            ], true)
    })
};