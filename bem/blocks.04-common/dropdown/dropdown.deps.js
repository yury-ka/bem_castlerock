[
    ({
        mustDeps: [
            {block: 'bootstrap'}
        ]
    }),
    ({
        shouldDeps: [
            {elem: ['toggle', 'block', 'list', 'li']}
        ]
    })
]