({
    mustDeps: [
        {block: 'core'},
        {block: 'checkbox-inline', elem: ['control', 'label']}
    ]
})
