([
    {
        mustDeps: [
            {block: 'bootstrap'},
        ]
    },
    {
        shouldDeps: [
            {block: 'a'},
            {block: 'fa'},
            {block: '$js-grid-point'},
            {block: 'link'},
            {block: 'spaces'},
            {block: 'type'},
            {block: 'jquery-eqheight'},
            {block: 'throttle-debounce'},
            {block: 'tooltip'}
        ]
    }
])
