([
    {
        shouldDeps : [
            {block: 'nav-tabs', elem: 'link'}
        ]
    },
    {
        mustDeps : [
            {block: 'bootstrap'}
        ]
    }
])
