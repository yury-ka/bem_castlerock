({
    mustDeps: [
        {block: 'breadcrumbs'},
        {block: 'breadcrumb', elem: ['item', 'link', 'active']}
    ]
})
