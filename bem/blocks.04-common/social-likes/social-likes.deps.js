({
    mustDeps: [
        { block: 'lazysizes' },
        { block: 'fa'}
    ],
    shouldDeps: [
        {elem: [
            'button',
            'counter',
            'icon',
            'widget'
        ]}
    ]
})