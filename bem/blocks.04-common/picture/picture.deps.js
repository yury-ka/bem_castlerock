({
    mustDeps: [
        {block: 'picturefill'},
        {block: 'bootstrap'},
        {block: 'conditional-comment'},
        {elems: 'source'},
    ]
})
