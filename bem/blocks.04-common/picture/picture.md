# picture

Блок реализует тег picture с полифилом https://scottjehl.github.io/picturefill/

## Обзор

### Специализированные поля блока

| Поле | Тип | Описание |
| ----------- | ------------------- | -------- |
| lg | String | URL изображения для использовании на разрешении 1200+. Для полноэкранного использования рекомендуется разрешение 1400-1600px |
| md | String | URL изображения для использовании на разрешении 992+. Для полноэкранного использования рекомендуется разрешение 1199px |
| sm | String | URL изображения для использовании на разрешении 768+. Для полноэкранного использования рекомендуется разрешение 991px |
| xs | String | URL изображения размером не менее 767px для использовании на разрешении 767-. Для полноэкранного использования рекомендуется разрешение 767px |

### Публичные технологии блока

Блок реализован в технологиях:

* `bh.js`
* `js`

## Подробности

Блок подключает скрипт picturefill, и добавляет в секцию head инлайн-скрипт для создания тега picture в старых браузерах.
На странице представляется как обычное изображение. В зависимости от разрешения экрана подставляет одно из изображений для минимизации объема трафика.
