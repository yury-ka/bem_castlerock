({
    mustDeps: [
        {block: 'cart-control', elem: ['minus', 'plus', 'input']},
        {block: 'cart-control', mod: 'wait'}
    ]
})
