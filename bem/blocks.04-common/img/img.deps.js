[
    ({
        mustDeps: [
            {block: 'bootstrap'}
        ]
    }),
    ({
        shouldDeps: [
            {mods: ['lazyload']}
        ]
    })
]