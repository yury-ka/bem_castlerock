module.exports = function (bh) {
    bh.match('img', function (ctx, json) {
        var $alt = json.alt || '';
        var $url = json.url || 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
        var urlPrefix = '';
        if (!json.web) {
            urlPrefix = '../../../images/';
        }
        ctx.tag('img')
            .attrs({
                src: urlPrefix + $url,
                alt: $alt
            })
    })
}
