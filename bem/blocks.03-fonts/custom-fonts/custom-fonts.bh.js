/**
 * Список опций "наблюдателя за загрузкой шрифта"
 * Каждый поднабор (файл шрифта) из CSS-файла необходимо описать отдельным элементом данного списка.
 *
 * Описание состоит из
 * 1 обязательного параметра:
 *  - Назание семейства шрифта
 *
 * и 3 опциональных:
 *  - Опции начертания https://github.com/bramstein/fontfaceobserver#how-to-use
 *      - Стиль
 *      - Толщина
 *      - Ширина
 *
 *  - Строка для проверки загруженности шрифта (по умолчанию BESbswy).
 *    Важно указывать в случае если шрифт содержит специфичные символы как в FontAwesome
 *    В случае если шрифт разбит на поднаборы по языкам рекомендуется использовать строку &#1081;&#1103;&#1046; для кирилицы.
 *
 *  - Время ожидания загрузки (милисекунды, по умолчанию 30 секунд) после которого происходит окончательный отказ в пользу системных шрифтов
 */

var customFonts = [
    //[
    //    'Open Sans',
    //    {
    //        style: 'normal',
    //        weight: 400,
    //        stretch: 'normal'
    //    },
    //    'BESbswy',
    //    3000
    //],
    //
    //['Open Sans', {
    //    style: 'normal',
    //    weight: 700
    //}],
    //['Open Sans', {
    //    style: 'normal',
    //    weight: 400
    //}],
];


module.exports = function (bh) {

    bh.match('deferred-head-strings', function (ctx, json) {
        var observerJson = JSON.stringify(customFonts);

        return [
            json,
            {
                tag: 'script',
                attrs: {'data-skip-moving': 'true'},
                content: [
                    '/* beautify preserve:start */',
                    '!function(){"use strict";for(var a=' + observerJson + ',b=[],c=0;c<a.length;c++){var d=a[c];d[1]=d[1]||{},d[2]=d[2]||null,d[3]=d[3]||3e4;var e=new FontFaceObserver(d[0],d[1]);b.push(e.load(d[2],d[3]))}b.length>0&&Promise.all(b).then(function(){document.documentElement.className+=" custom-fonts-loaded"})}();',
                    '/* beautify preserve:end */'
                ]
            }
        ];
    });

};

// Исходный скрипт наблюдателя
//(function () {
//    'use strict';
//
//    var customFonts = [
//        // insert custom font config
//    ];
//
//    var promices = [];
//    for (var i = 0; i < customFonts.length; i++) {
//        var observerArgs = customFonts[i];
//        observerArgs[1] = observerArgs[1] || {};
//        observerArgs[2] = observerArgs[2] || null;
//        observerArgs[3] = observerArgs[3] || 30000;
//
//        var observer = new FontFaceObserver(observerArgs[0], observerArgs[1]);
//        promices.push(observer.load(observerArgs[2], observerArgs[3]));
//    }
//
//    if (promices.length > 0) {
//        Promise.all(promices).then(function () {
//            document.documentElement.className += " custom-fonts-loaded";
//        });
//    }
//})();
